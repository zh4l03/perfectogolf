/*
Name: 			Tables / Advanced - Examples
Written by: 	Okler Themes - (http://www.okler.net)
Theme Version: 	4.0.0
*/

(function($) {

	'use strict';

	var datatableInit = function() {

		$('#tabelkasir').dataTable({
			select: true,
			"searching": false,
			"ordering": false,
			"paging": false,
			dom: '<"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p'
		});

		$('#tabelcariproduk').dataTable({
			"paging": false,
			dom: '<"row"<"col-lg-6"l><"col-lg-6"f>><"table-responsive"t>p'
		});


	};

	$(function() {
		datatableInit();
	});

}).apply(this, [jQuery]);