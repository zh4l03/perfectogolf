<div class="inner-wrapper">
    <div class="tab-navigation collapse">
        <nav>
            <ul class="nav nav-pills">
                <li class="">
                    <a class="nav-link" href="<?= base_url() ?>">
                        <i class="fas fa-home" aria-hidden="true"></i>Dashboard
                    </a>
                </li>
                <li class="">
                    <a class="nav-link" href="#">
                        <i class="fas fa-home" aria-hidden="true"></i>Data Produk
                    </a>
                </li>
                <li class="">
                    <a class="nav-link" href="#">
                        <i class="fas fa-home" aria-hidden="true"></i>Data Transaksi
                    </a>
                </li>
                <li class="dropdown">
                    <a class="nav-link dropdown-toggle" href="#">
                        <i class="fas fa-shopping-cart" aria-hidden="true"></i>Laporan
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a class="nav-link" href="#">
                                Laporan Transaksi
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="#">
                                Laporan Keuangan
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a class="nav-link" href="<?= base_url('kasir') ?>">
                        <i class="fas fa-home" aria-hidden="true"></i>Kasir
                    </a>
                </li>
            </ul>
        </nav>
    </div>