<?php
$dataHeader['title'] = "Kasir";
$this->load->view('layout/heading', $dataHeader);

$this->load->view('layout/header');

$this->load->view('layout/sidebar');
?>

<section role="main" class="content-body tab-menu-opened">
    <div class="row">
        <!-- <div class="input-group mb-3">
            <input type="text" class="form-control">
            <button class="btn btn-success" type="button">Go!</button>
        </div> -->

        <div class="col-12">
            <p style="text-align: right;">
                <a class="mb-1 mt-1 me-1 modal-with-zoom-anim ws-normal btn btn-primary btn-sm" href="#cariproduk"><i class="icon icon-magnifier-add"></i> Cari Produk</a>&nbsp;|&nbsp;
                <a class="btn btn-warning btn-sm" href="<?= base_url('/trading') ?>"><i class="icon icon-directions"></i> Trading</a>&nbsp;|&nbsp;
                <button class="btn btn-sm btn-secondary"><i class="icon icon-bag"></i> Beli Barang</button>
            </p>
        </div>
        <hr>
        <div class="col-12">
            <table class="table table-bordered table-striped mb-0" id="tabelkasir">
                <thead>
                    <tr>
                        <th>Kode Produk</th>
                        <th>Nama Produk</th>
                        <th style="text-align: center;">Jumlah</th>
                        <th style="text-align: right;">Harga Satuan</th>
                        <th style="text-align: right;">Total Harga</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Trident</td>
                        <td>Internet Explorer 4.0</td>
                        <td style="text-align: center;"><input type="number" class="form-control form-control-sm" style="text-align: center;" value="2" /></td>
                        <td style="text-align: right;"><input type="text" class="form-control form-control-sm" style="text-align: right;" value="1.500.000" /></td>
                        <td style="text-align: right;">3.000.000</td>
                    </tr>
                    <tr>
                        <td>Trident</td>
                        <td>Internet Explorer 4.0</td>
                        <td style="text-align: center;" width="80"><input type="number" class="form-control form-control-sm" style="text-align: center;" value="2" /></td>
                        <td style="text-align: right;" width="150"><input type="text" class="form-control form-control-sm" style="text-align: right;" value="1.500.000" /></td>
                        <td style="text-align: right;" width="150">8.000.000</td>
                    </tr>
                </tbody>
            </table><br>
            <table class="table table-bordered">
                <tr>
                    <td columnspan="4" style="text-align: right;"><b>SUBTOTAL</b></td>
                    <td style="text-align: right;" width="165"><b>asd</b></td>
                </tr>
                <tr>
                    <td columnspan="4" style="text-align: right;"><b>Diskon</b></td>
                    <td style="text-align: right;" width="165"><b>asd</b></td>
                </tr>
                <tr>
                    <td columnspan="4" style="text-align: right;"><b>TOTAL BAYAR</b></td>
                    <td style="text-align: right;" width="165"><b>asd</b></td>
                </tr>
            </table>
        </div>
    </div>
</section>

<?php $this->load->view('layout/footer'); ?>


<div id="cariproduk" class="zoom-anim-dialog modal-block modal-block-primary mfp-hide" role="dialog">
    <section class="card">
        <header class="card-header">
            <h2 class="card-title">Daftar Produk</h2>
        </header>
        <div class="card-body">
            <div class="modal-wrapper">
                <div class="modal-text">
                    <p class="mb-0">
                    <table class="table table-bordered table-striped mb-0" id="tabelcariproduk">
                        <thead>
                            <th style="text-align: center;">Pilih</th>
                            <th>Kode Produk</th>
                            <th>Nama Produk</th>
                            <th style="text-align: right;">Harga</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="text-align: center;"><input type="checkbox" /></td>
                                <td>Trident</td>
                                <td>Internet Explorer 4.0</td>
                                <td style="text-align: right;">1.500.000</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;" width="10"><input type="checkbox" /></td>
                                <td>Trident</td>
                                <td>Internet Explorer 4.0</td>
                                <td style="text-align: right;" width="20">4.000.000</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;"><input type="checkbox" /></td>
                                <td>Trident</td>
                                <td>Internet Explorer 4.0</td>
                                <td style="text-align: right;">2.000.000</td>
                            </tr>
                        </tbody>
                    </table>
                    </p>
                </div>
            </div>
        </div>
        <footer class="card-footer">
            <div class="row">
                <div class="col-md-12 text-end">
                    <button class="btn btn-primary btn-sm modal-confirm"><i class="icon icon-plus"></i> Tambahkan</button>
                    <button class="btn btn-warning btn-sm modal-dismiss">Batal</button>
                </div>
            </div>
        </footer>
    </section>
</div>



</body>

</html>