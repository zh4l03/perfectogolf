<?php
$dataHeader['title'] = "Pergudangan | Data Barang Masuk";
$this->load->view('layout/heading', $dataHeader);

$this->load->view('layout/header');

$this->load->view('layout/sidebar');
?>

<section role="main" class="content-body">
    <header class="page-header">
        <h2>Barang Masuk</h2>
        <div class="right-wrapper text-end">
            <ol class="breadcrumbs">
                <li>
                    <a href="<?= base_url() ?>">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li><span>Table</span></li>
                <li><span>Barang Masuk</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
        </div>
    </header>
    <div class="row">
        <div class="col-12">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <!-- <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a> -->
                    </div>
                    <h2 class="card-title"><?php if ($this->uri->segment(2) == "tambah") { echo "Tambah "; }else{ echo "Data "; } ?>Barang Masuk</h2>
                </header>

                <!-- ----------------------------Tambah Barang masuk---------------------------- -->
                <?php
                    if ($this->uri->segment(2) == "tambah") {
                ?>
                    <!-- <hr> -->
                    <div class="card-body">
                        <div class="row">
                            <form class="form-horizontal form-bordered" method="get">
                                <div class="form-group"></div>

                                <div class="form-group row pb-2">
                                    <label class="col-lg-3 control-label text-lg-end pt-2">No Invoice</label>
                                    <div class="col-sm-8">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <input type="text" placeholder="no invoice/tanda terima dari suplier" class="form-control" value="">
                                            </div>
                                            <div class="d-md-none mb-3"></div>
                                            <div class="col-lg-2">
                                                <label class="control-label text-lg-end pt-2" for="inputDisabled">Tanggal</label>
                                            </div>
                                            <div class="d-md-none mb-2"></div>
                                            <div class="col-sm-4">
                                                <div class="input-group">
                                                    <span class="input-group-text">
                                                        <i class="fas fa-calendar-alt"></i>
                                                    </span>
                                                    <input type="text" data-plugin-datepicker class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row pb-2">
                                    <label class="col-lg-3 control-label text-lg-end pt-2" for="inputDisabled">Nama Barang</label>
                                    <div class="col-lg-6">
                                        <select data-plugin-selectTwo class="form-control populate">
                                            <option>-- Pilih Barang --</option>
                                            <option value="AZ">Arizona</option>
                                            <option value="CO">Colorado</option>
                                            <option value="ID">Idaho</option>
                                            <option value="MT">Montana</option>
                                            <option value="NE">Nebraska</option>
                                            <option value="NM">New Mexico</option>
                                            <option value="ND">North Dakota</option>
                                            <option value="UT">Utah</option>
                                            <option value="WY">Wyoming</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row pb-2">
                                    <label class="col-lg-3 control-label text-lg-end pt-2">Harga Barang</label>
                                    <div class="col-sm-8">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <input type="text" placeholder="Harga satuan dari suplier" class="form-control" value="">
                                            </div>
                                            <div class="d-md-none mb-3"></div>
                                            <div class="col-lg-1 mb-3">
                                                <label class="control-label text-lg-end pt-2" for="inputDisabled">Qty</label>
                                            </div>
                                            <div class="d-md-none mb-2"></div>
                                            <div class="col-sm-4">
                                                <input type="number" class="form-control" placeholder="Qty" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row pb-2">
                                    <label class="col-lg-3 control-label text-lg-end pt-2">Sumber Barang</label>
                                    <div class="col-sm-8">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <select data-plugin-selectTwo class="form-control">
                                                    <option>- Pilih Sumber -</option>
                                                    <option value="AZ">Suplier</option>
                                                    <option value="CO">Trading</option>
                                                    <option value="ID">Pembelian</option>
                                                </select>
                                            </div>
                                            <div class="d-md-none mb-3"></div>
                                            <div class="col-lg-2 mb-3">
                                                <label class="control-label text-lg-end pt-2" for="inputDisabled">Lokasi Drop</label>
                                            </div>
                                            <div class="d-md-none mb-3"></div>
                                            <div class="col-sm-4">
                                                <select data-plugin-selectTwo class="form-control populate">
                                                    <option>-- Pilih Store --</option>
                                                    <option value="AZ">Arizona</option>
                                                    <option value="CO">Colorado</option>
                                                    <option value="ID">Idaho</option>
                                                    <option value="MT">Montana</option>
                                                    <option value="NE">Nebraska</option>
                                                    <option value="NM">New Mexico</option>
                                                    <option value="ND">North Dakota</option>
                                                    <option value="UT">Utah</option>
                                                    <option value="WY">Wyoming</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row pb-2">
                                    <label class="col-lg-3 control-label text-lg-end pt-2" for="inputNoInvoice">Keterangan</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" id="inputNoInvoice">
                                    </div>
                                </div>
                                <div class="form-group row pb-2">
                                    <label class="col-lg-3 control-label text-lg-end pt-2" for="inputNoInvoice"></label>
                                    <div class="col-lg-6">
                                        <button class="btn btn-primary btn-sm"><i class="el el-plus"></i> Tambahkan</button>
                                        <a href="<?= base_url('barangmasuk') ?>" class="btn btn-secondary btn-sm"><i class="el el-return-key"></i> Batal</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <hr>
                <?php } else {
                    echo "";
                } ?>
                <!-- ---------------------------- ++ END ++ ---------------------------- -->

                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12 form-group">
                            <label class="label-control">
                                <h4>Filter</h4>
                            </label>
                        </div>
                        <div class="col-sm-3 form-group pb-2">
                            <input type="text" value="" class="form-control" placeholder="No Invoice" />
                        </div>
                        <div class="col-sm-3 form-group pb-2">
                            <input type="text" value="" class="form-control" placeholder="Nama Produk" />
                        </div>
                        <div class="col-sm-3 form-group pb-2">
                            <select data-plugin-selectTwo class="form-control form-control-sm populate">
                                <optgroup label="Kategori Produk">
                                    <option>Semua Kategori</option>
                                    <option value="AZ">Arizona</option>
                                    <option value="CO">Colorado</option>
                                    <option value="ID">Idaho</option>
                                    <option value="MT">Montana</option>
                                    <option value="NE">Nebraska</option>
                                    <option value="NM">New Mexico</option>
                                    <option value="ND">North Dakota</option>
                                    <option value="UT">Utah</option>
                                    <option value="WY">Wyoming</option>
                            </select>
                        </div>
                        <div class="col-sm-3 form-group pb-2">
                            <select data-plugin-selectTwo class="form-control form-control-sm populate">
                                <optgroup label="Sumber Barang">
                                    <option value="">Semua Sumber Barang</option>
                                    <option value="">Suplier</option>
                                    <option value="">Pembelian</option>
                                    <option value="">Trading</option>
                            </select>
                        </div>
                        <div class="col-sm-6 form-group" style="text-align: right;"></div>
                        <div class="col-sm-6 form-group" style="text-align: right;">
                            <?php
                                if ($this->uri->segment(2) == "tambah") { echo ""; 
                            ?>
                               
                            <?php } else { ?>
                                <a href="<?= base_url('barangmasuk/tambah') ?>" class="mb-1 mt-1 me-1 btn btn-success"><i class="el el-file-new"></i> Tambah Barang</a>
                            <?php } ?>
                            <button class="mb-1 mt-1 me-1 btn btn-primary"><i class="icons icon-magnifier"></i> Cari</button>
                        </div>
                    </div>
                </div>
                
                <div class="card-body">
                    <table class="table table-bordered table-stiped" id="tabelbarangmasuk">
                        <thead>
                            <th style="text-align: center;">NO</th>
                            <th>NO INVOICE</th>
                            <th>NAMA BARANG</th>
                            <th style="text-align: center;">QTY</th>
                            <th>SUMBER BARANG</th>
                            <th>KETERANGAN</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="text-align: center;">1</td>
                                <td>HS/231/JKH/123</td>
                                <td>Driver Golf Stick</td>
                                <td style="text-align: center;">5</td>
                                <td>Suplier</td>
                                <td>Ehhmmm</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">1</td>
                                <td>HS/231/JKH/123</td>
                                <td>Driver Golf Stick</td>
                                <td style="text-align: center;">5</td>
                                <td>Suplier</td>
                                <td>Ehhmmm</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">1</td>
                                <td>HS/231/JKH/123</td>
                                <td>Driver Golf Stick</td>
                                <td style="text-align: center;">5</td>
                                <td>Suplier</td>
                                <td>Ehhmmm</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">1</td>
                                <td>HS/231/JKH/123</td>
                                <td>Driver Golf Stick</td>
                                <td style="text-align: center;">5</td>
                                <td>Suplier</td>
                                <td>Ehhmmm</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">1</td>
                                <td>HS/231/JKH/123</td>
                                <td>Driver Golf Stick</td>
                                <td style="text-align: center;">5</td>
                                <td>Suplier</td>
                                <td>Ehhmmm</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">1</td>
                                <td>HS/231/JKH/123</td>
                                <td>Driver Golf Stick</td>
                                <td style="text-align: center;">5</td>
                                <td>Suplier</td>
                                <td>Ehhmmm</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">1</td>
                                <td>HS/231/JKH/123</td>
                                <td>Driver Golf Stick</td>
                                <td style="text-align: center;">5</td>
                                <td>Suplier</td>
                                <td>Ehhmmm</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">1</td>
                                <td>HS/231/JKH/123</td>
                                <td>Driver Golf Stick</td>
                                <td style="text-align: center;">5</td>
                                <td>Suplier</td>
                                <td>Ehhmmm</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">1</td>
                                <td>HS/231/JKH/123</td>
                                <td>Driver Golf Stick</td>
                                <td style="text-align: center;">5</td>
                                <td>Suplier</td>
                                <td>Ehhmmm</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">1</td>
                                <td>HS/231/JKH/123</td>
                                <td>Driver Golf Stick</td>
                                <td style="text-align: center;">5</td>
                                <td>Suplier</td>
                                <td>Ehhmmm</td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">1</td>
                                <td>HS/231/JKH/123</td>
                                <td>Driver Golf Stick</td>
                                <td style="text-align: center;">5</td>
                                <td>Suplier</td>
                                <td>Ehhmmm</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
</section>

<?php $this->load->view('layout/footer'); ?>

</body>

</html>