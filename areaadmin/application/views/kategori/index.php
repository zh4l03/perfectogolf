<?php
$dataHeader['title'] = "Master | Data Kategori";
$this->load->view('layout/heading', $dataHeader);

$this->load->view('layout/header');

$this->load->view('layout/sidebar');
?>

<section role="main" class="content-body">
    <header class="page-header">
        <h2>Data Kategori</h2>
        <div class="right-wrapper text-end">
            <ol class="breadcrumbs">
                <li>
                    <a href="index.html">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li><span>Table</span></li>
                <li><span>Kategori</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
        </div>
    </header>
    <div class="row">
        <div class="col">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Data Katagori</h2>
                </header>
                <div class="card-body">
                    <div class="float-right">
                        <button type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" class="btn-sm mb-1 mt-1 me-1 btn btn-primary btn pull-right">
                            <i class="el el-plus"></i>&nbsp; Tambah Kategori
                        </button>
                    </div> <br />
                    <hr />
                    <div class="collapse" id="collapseExample">
                        <div class="card card-body">
                            <form class="form-horizontal form-bordered" enctype="multipart/form-data" method="post" action="<?= base_url() ?>/kategori/add">
                                <div class="form-group row">
                                    <label class="col-lg-2 control-label text-lg-end pt-2" for="kodeKategori">Kode Kategori</label>
                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" id="kodekategori" name="kodekategori" maxlength="3" required >
                                    </div>
                                    <label class="col-lg-2 control-label text-lg-end pt-2" for="namaKategori">Nama Kategori</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" id="namakategori" name="namakategori" required>
                                    </div>
                                    <div class="col-lg-2">
                                        <button class="btn btn-primary" type="submit" name="action" value="save">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <br />
                    </div>

                    <table class="table table-bordered table-striped mb-0 display nowrap" id="datatables">
                        <thead>
                            <tr>
                                <th>Kode Kategori</th>
                                <th>Nama Kategori</th>
                                <th>Tanggal Update</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($kategori->result_array() as $row) { ?>
                                <tr>
                                    <td><?= $row['kodekategori'] ?></td>
                                    <td><?= $row['namakategori'] ?></td>
                                    <td><?= $row['logtgl'] ?></td>
                                    <td>
                                        <a class="mb-1 mt-1 me-1 modal-basic btn btn-sm btn-warning" href="#editkategori<?= $row['kodekategori'] ?>"><i class="el el-pencil"></i></a>
                                        <a class="mb-1 mt-1 me-1 modal-basic btn btn-sm btn-danger" href="#deletekategori<?= $row['kodekategori'] ?>"><i class="el el-trash"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!-- Modal -->
                    <?php foreach ($kategori->result_array() as $row) { ?>

                        <div id="deletekategori<?= $row['kodekategori'] ?>" class="modal-block modal-block-primary mfp-hide">
                            <form class="form-horizontal form-bordered" enctype="multipart/form-data" method="post" action="<?= base_url() ?>/kategori/delete">
                                <section class="card">
                                    <header class="card-header">
                                        <h2 class="card-title">Hapus Data</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="modal-wrapper">
                                            <div class="modal-icon">
                                                <i class="fas fa-question-circle"></i>
                                            </div>
                                            <div class="modal-text">
                                                <input type="text" name="id" value="<?= $row['kodekategori'] ?>" hidden />
                                                <p>Apakah Anda Yakin Ingin Menghapus Data <?= $row['namakategori'] ?> ?</p>
                                            </div>
                                        </div>
                                    </div>
                                    <footer class="card-footer">
                                        <div class="row">
                                            <div class="col-md-12 text-end">
                                                <button class="btn btn-primary" type="submit" name="action" value="delete">Hapus</button>
                                                <button class="btn btn-default modal-dismiss">Batal</button>
                                            </div>
                                        </div>
                                    </footer>
                                </section>
                            </form>
                        </div>

                        <div id="editkategori<?= $row['kodekategori'] ?>" class="modal-block modal-block-primary mfp-hide">
                            <form class="form-horizontal form-bordered" enctype="multipart/form-data" method="post" action="<?= base_url() ?>/kategori/edit">
                                <section class="card">
                                    <header class="card-header">
                                        <h2 class="card-title">Edit Data</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="modal-wrapper">
                                            <div class="modal-text">
                                                <input type="text" name="id" value="<?= $row['kodekategori'] ?>" hidden />
                                                <div class="form-group row">
                                                    <div class="form-group row pb-4">
                                                        <label class="col-lg-3 control-label text-lg-end pt-2" for="kodeKategori">Kode Kategori</label>
                                                        <div class="col-lg-6">
                                                            <input type="text" class="form-control" id="kodelama" name="kodelama" value="<?= $row['kodekategori'] ?>" hidden>
                                                            
                                                            <?php $inputkode = $this->db->query("select kodekategori from tbproduk where kodekategori='".$row['kodekategori']."'")->num_rows(); 
                                                                $read = "";
                                                                if($inputkode>0){
                                                                    $read="readonly";
                                                                }
                                                            ?>
                            
                                                            <input type="text" class="form-control" id="kodekategori" name="kodekategori" value="<?= $row['kodekategori'] ?>" maxlength="3" required <?=$read?>>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row pb-4">
                                                        <label class="col-lg-3 control-label text-lg-end pt-2" for="namaKategori">Nama Kategori</label>
                                                        <div class="col-lg-6">
                                                            <input type="text" class="form-control" id="namakategori" name="namakategori" value="<?= $row['namakategori'] ?>" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <footer class="card-footer">
                                        <div class="row">
                                            <div class="col-md-12 text-end">
                                                <button class="btn btn-primary" type="submit" name="action" value="edit">Edit</button>
                                                <button class="btn btn-default modal-dismiss">Batal</button>
                                            </div>
                                        </div>
                                    </footer>
                                </section>
                            </form>
                        </div>

                    <?php } ?>
                </div>
            </section>
        </div>
    </div>
</section>

<?php $this->load->view('layout/footer'); ?>

</body>

</html>