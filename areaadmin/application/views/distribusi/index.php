<?php
$dataHeader['title'] = "Master | Data Produk";
$this->load->view('layout/heading', $dataHeader);

$this->load->view('layout/header');

$this->load->view('layout/sidebar');
?>

<section role="main" class="content-body">
    <header class="page-header">
        <h2>Data Produk</h2>
        <div class="right-wrapper text-end">
            <ol class="breadcrumbs">
                <li>
                    <a href="index.html">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li><span>Table</span></li>
                <li><span>Produk</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
        </div>
    </header>
    <div class="row">
        <div class="col">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Data Produk</h2>
                </header>
                <div class="card-body">

                </div>
            </section>
        </div>
    </div>
</section>

<?php $this->load->view('layout/footer'); ?>

</body>

</html>