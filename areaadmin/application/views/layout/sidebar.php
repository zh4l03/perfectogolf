<div class="inner-wrapper">
    <!-- start: sidebar -->
    <aside id="sidebar-left" class="sidebar-left">

        <div class="sidebar-header">
            <div class="sidebar-title">
                Main Menu
            </div>
            <div class="sidebar-toggle d-none d-md-block" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
            </div>
        </div>

        <div class="nano">
            <div class="nano-content">
                <nav id="menu" class="nav-main" role="navigation">

                    <ul class="nav nav-main">
                        <li>
                            <a class="nav-link" href="<?= base_url() ?>">
                                <i class="bx bx-home-alt" aria-hidden="true"></i>
                                <span>Home</span>
                            </a>
                        </li>

                        <li class="nav-parent">
                            <a class="nav-link" href="#">
                                <i class="bx bx-cube"></i>
                                <span>Data Produk</span>
                            </a>
                            <ul class="nav nav-children">
                                <li>
                                    <a class="nav-link" href="<?= base_url() ?>kategori">
                                        Kategori
                                    </a>
                                </li>

                                <li>
                                    <a class="nav-link" href="<?= base_url() ?>produk">
                                        Produk
                                    </a>
                                </li>

                            </ul>
                        </li>

                        <li class="nav-parent">
                            <a class="nav-link" href="#">
                                <i class="icons icon-share-alt"></i>
                                <span>Pergudangan</span>
                            </a>
                            <ul class="nav nav-children">
                                <li>
                                    <a class="nav-link" href="<?= base_url('barangmasuk') ?>">
                                        Barang Masuk
                                    </a>
                                </li>
                                <li>
                                    <a class="nav-link" href="<?= base_url('distribusi') ?>">
                                        Distribusi Barang
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-parent">
                            <a class="nav-link" href="#">
                                <i class="bx bx-detail"></i>
                                <span>Data Transaksi</span>
                            </a>
                            <ul class="nav nav-children">
                                <li>
                                    <a class="nav-link" href="<?= base_url() ?>kategori">
                                        Daftar Transaksi
                                    </a>
                                </li>

                            </ul>
                        </li>
                        <li class="nav-parent">
                            <a class="nav-link" href="#">
                                <i class="icons icon-people"></i>
                                <span>User Management</span>
                            </a>
                            <ul class="nav nav-children">
                                <li>
                                    <a class="nav-link" href="<?= base_url() ?>store/index">
                                        Store
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-children">
                                <li>
                                    <a class="nav-link" href="<?= base_url() ?>department">
                                        Department
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-children">
                                <li>
                                    <a class="nav-link" href="<?= base_url() ?>jabatan">
                                        Jabatan
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-children">
                                <li>
                                    <a class="nav-link" href="<?= base_url() ?>pegawai">
                                        Pegawai
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>

            <script>
                // Maintain Scroll Position
                if (typeof localStorage !== 'undefined') {
                    if (localStorage.getItem('sidebar-left-position') !== null) {
                        var initialPosition = localStorage.getItem('sidebar-left-position'),
                            sidebarLeft = document.querySelector('#sidebar-left .nano-content');

                        sidebarLeft.scrollTop = initialPosition;
                    }
                }
            </script>

        </div>

    </aside>
    <!-- end: sidebar -->