<?php
$dataHeader['title'] = "Master | Data Store";
$this->load->view('layout/heading', $dataHeader);

$this->load->view('layout/header');

$this->load->view('layout/sidebar');
?>

<section role="main" class="content-body">
    <header class="page-header">
        <h2>Data Store</h2>
        <div class="right-wrapper text-end">
            <ol class="breadcrumbs">
                <li>
                    <a href="<?= base_url() ?>">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li><span>Table</span></li>
                <li><span>Store</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
        </div>
    </header>
    <div class="row">
        <div class="col">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Data Store</h2>
                </header>
                <div class="card-body">

                    <div class="card-body">

                        <!-- <div class="float-right">
                            <button type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" class="btn-sm mb-1 mt-1 me-1 btn btn-primary btn pull-right">
                                <i class="el el-plus"></i>&nbsp; Tambah Store
                            </button>
                        </div> <br />
                        <hr /> -->

                        <div class="collapse" id="collapseExample">
                            <div class="card card-body">
                                <form class="form-horizontal form-bordered" enctype="multipart/form-data" method="post" action="<?= base_url() ?>/store/add">
                                    <div class="form-group row pb-4">
                                        <label class="col-lg-3 control-label text-lg-end pt-2" for="kodestore">Kode Store</label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" id="kodestore" name="kodestore" maxlength="5" required>
                                        </div>
                                    </div>
                                    <div class="form-group row pb-4">
                                        <label class="col-lg-3 control-label text-lg-end pt-2" for="wilayah">Wilayah</label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" id="wilayah" name="wilayah" required>
                                        </div>
                                    </div>
                                    <div class="form-group row pb-4">
                                        <label class="col-lg-3 control-label text-lg-end pt-2" for="alamat">Alamat</label>
                                        <div class="col-lg-6">
                                            <Textarea class="form-control" name="alamat" cols="100" rows="4" required></Textarea>
                                        </div>
                                    </div>

                                    <footer class="card-footer">
                                        <div class="row justify-content-end">
                                            <div class="col-sm-9">
                                                <button class="btn btn-primary" type="submit" name="action" value="save">Simpan</button>
                                            </div>
                                    </footer>
                                </form>
                            </div>
                            <br />
                        </div>

                        <table class="table table-bordered table-striped mb-0 display nowrap" id="datatables">
                            <thead>
                                <tr>
                                    <th>Kode Store</th>
                                    <th>Wilayah</th>
                                    <th>Alamat</th>
                                    <th>Tanggal Update</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($store->result_array() as $row) { ?>
                                    <tr>
                                        <td><?= $row['kodestore'] ?></td>
                                        <td><?= $row['wilayah'] ?></td>
                                        <td><?= $row['alamat'] ?></td>
                                        <td><?= $row['logtgl'] ?></td>
                                        <td>
                                            <a class="mb-1 mt-1 me-1 modal-basic btn btn-sm btn-warning" href="#editstore<?= $row['kodestore'] ?>"><i class="el el-pencil"></i></a>
                                            <a class="mb-1 mt-1 me-1 modal-basic btn btn-sm btn-danger" href="#deletestore<?= $row['kodestore'] ?>"><i class="el el-trash"></i></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <!-- Modal Delete -->
                        <?php foreach ($store->result_array() as $row) { ?>

                            <div id="deletestore<?= $row['kodestore'] ?>" class="modal-block modal-block-primary mfp-hide">
                                <form class="form-horizontal form-bordered" enctype="multipart/form-data" method="post" action="<?= base_url() ?>/store/delete">
                                    <section class="card">
                                        <header class="card-header">
                                            <h2 class="card-title">Hapus Data</h2>
                                        </header>
                                        <div class="card-body">
                                            <div class="modal-wrapper">
                                                <div class="modal-icon">
                                                    <i class="fas fa-question-circle"></i>
                                                </div>
                                                <div class="modal-text">
                                                    <input type="text" name="id" value="<?= $row['kodestore'] ?>" hidden />
                                                    <p>Apakah Anda Yakin Ingin Menghapus Data <?= $row['wilayah'] ?> ?</p>
                                                </div>
                                            </div>
                                        </div>
                                        <footer class="card-footer">
                                            <div class="row">
                                                <div class="col-md-12 text-end">
                                                    <button class="btn btn-primary" type="submit" name="action" value="delete">Hapus</button>
                                                    <button class="btn btn-default modal-dismiss">Batal</button>
                                                </div>
                                            </div>
                                        </footer>
                                    </section>
                                </form>
                            </div>

                            <div id="editstore<?= $row['kodestore'] ?>" class="modal-block modal-block-primary mfp-hide">
                                <form class="form-horizontal form-bordered" enctype="multipart/form-data" method="post" action="<?= base_url() ?>/store/edit">
                                    <section class="card">
                                        <header class="card-header">
                                            <h2 class="card-title">Edit Data</h2>
                                        </header>
                                        <div class="card-body">
                                            <div class="modal-wrapper">
                                                <div class="modal-text">
                                                    <input type="text" name="id" value="<?= $row['kodestore'] ?>" hidden />
                                                    <div class="form-group row">
                                                        <div class="form-group row pb-4">
                                                            <label class="col-lg-3 control-label text-lg-end pt-2" for="kodestore">Kode Store</label>
                                                            <div class="col-lg-6">
                                                                <?php 
                                                                $inputkode1 = $this->db->query("select kodestore from tbproduk where kodestore='" . $row['kodestore'] . "'")->num_rows();
                                                                $inputkode2 = $this->db->query("select kodestore from tbpegawai where kodestore='" . $row['kodestore'] . "'")->num_rows();
                                                                $read = "";
                                                                if ($inputkode1 > 0 || $inputkode2 > 0) {
                                                                    $read = "readonly";
                                                                }
                                                                ?>
                                                                <input type="text" class="form-control" id="kodelama" name="kodelama" value="<?= $row['kodestore'] ?>" hidden>
                                                                <input type="text" class="form-control" id="kodestore" name="kodestore" value="<?= $row['kodestore'] ?>" maxlength="2" required <?=$read?>>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row pb-4">
                                                            <label class="col-lg-3 control-label text-lg-end pt-2" for="wilayah">Wilayah</label>
                                                            <div class="col-lg-6">
                                                                <input type="text" class="form-control" id="wilayah" name="wilayah" value="<?= $row['wilayah'] ?>" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row pb-4">
                                                            <label class="col-lg-3 control-label text-lg-end pt-2" for="alamat">Alamat</label>
                                                            <div class="col-lg-6">
                                                                <Textarea class="form-control" name="alamat" cols="100" rows="4" required><?= $row['alamat'] ?></Textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <footer class="card-footer">
                                            <div class="row">
                                                <div class="col-md-12 text-end">
                                                    <button class="btn btn-primary" type="submit" name="action" value="edit">Edit</button>
                                                    <button class="btn btn-default modal-dismiss">Batal</button>
                                                </div>
                                            </div>
                                        </footer>
                                    </section>
                                </form>
                            </div>
                        <?php } ?>
                    </div>
            </section>
        </div>
    </div>
</section>

<?php $this->load->view('layout/footer'); ?>

</body>

</html>