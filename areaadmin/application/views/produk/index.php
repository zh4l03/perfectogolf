<?php
$dataHeader['title'] = "Master | Data Produk";
$this->load->view('layout/heading', $dataHeader);

$this->load->view('layout/header');

$this->load->view('layout/sidebar');
?>

<section role="main" class="content-body">
    <header class="page-header">
        <h2>Data Produk</h2>
        <div class="right-wrapper text-end">
            <ol class="breadcrumbs">
                <li>
                    <a href="index.html">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li><span>Table</span></li>
                <li><span>Produk</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
        </div>
    </header>
    <div class="row">
        <div class="col">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <!-- <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a> -->
                    </div>

                    <h2 class="card-title">Data Produk</h2>
                </header>
                <div class="card-body">

                    <div class="float-right">
                        <button type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" class="btn-sm mb-1 mt-1 me-1 btn btn-primary btn pull-right">
                            <i class="el el-plus"></i>&nbsp; Tambah Produk
                        </button>
                    </div> <br />
                    <hr />

                    <div class="collapse" id="collapseExample">
                        <div class="card card-body">
                            <form class="form-horizontal form-bordered" enctype="multipart/form-data" method="post" action="<?= base_url() ?>/produk/add">
                                <div class="form-group row pb-2">
                                    <label class="col-lg-3 control-label text-lg-end pt-2" for="kodeproduk">Kode Produk</label>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control form-control-sm" id="kodeproduk" name="kodeproduk" readonly>
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="checkbox-inline">
                                            <div class="checkbox-custom checkbox-default">
                                                <input type="checkbox" checked="" name="kodestatus" id="kodestatus" onclick="ubahstatus()" maxlength="12">
                                                <label for="kodestatus">Otomatis</label>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group row pb-2">
                                    <label class="col-lg-3 control-label text-lg-end pt-2" for="namaproduk">Nama Produk</label>
                                    <div class="col-lg-6">
                                        <input type="text" placeholder="Nama Produk" class="form-control form-control-sm" id="namaproduk" name="namaproduk" required>
                                    </div>
                                </div>
                                <div class="form-group row pb-2">
                                    <label class="col-lg-3 control-label text-lg-end pt-2" for="kategori">Kategori</label>
                                    <div class="col-sm-8">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <select name="kodekategori" class="form-control form-control-sm" required>
                                                    <option value="">-- Pilih Kategori --</option>
                                                    <?php $kategoridata = $this->db->query("select kodekategori,namakategori from tbkategori")->result_array();
                                                    foreach ($kategoridata as $kategori) {
                                                    ?>
                                                        <option value="<?= $kategori['kodekategori'] ?>"><?= $kategori['kodekategori'].' - '.$kategori['namakategori'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="d-md-none mb-3"></div>
                                            <div class="col-lg-2 mb-3">
                                                <label class="control-label text-lg-end pt-2" for="inputDisabled">Unit Produk</label>
                                            </div>
                                            <div class="d-md-none mb-3"></div>
                                            <div class="col-sm-3">
                                                <select class="form-control form-control-sm" name="unit" required>
                                                    <option value="">-- Pilih Unit --</option>
                                                    <option value="S">S - Second</option>
                                                    <option value="N">N - New</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="form-group row pb-2">
                                    <label class="col-lg-3 control-label text-lg-end pt-2" for="kategori">Kategori</label>
                                    <div class="col-lg-6">
                                        <select name="kodekategori" class="form-control form-control-sm">
                                            <option>Pilih Kategori Produk</option>
                                            <?php //$kategoridata = $this->db->query("select kodekategori,namakategori from tbkategori")->result_array();
                                            //foreach ($kategoridata as $kategori) {
                                            ?>
                                                <option value="<?//= $kategori['kodekategori'] ?>"><?//= $kategori['namakategori'] ?></option>
                                            <?php //} ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row pb-2">
                                    <label class="col-lg-3 control-label text-lg-end pt-2" for="Store">Store</label>
                                    <div class="col-lg-6">
                                        <select name="kodestore" class="form-control form-control-sm">
                                            <option>Pilih Store</option>
                                            <?php //$storedata = $this->db->query("select kodestore,wilayah from tbstore")->result_array();
                                            //foreach ($storedata as $store) {
                                            ?>
                                                <option value="<? //= $store['kodestore'] 
                                                                ?>"><? //= $store['wilayah'] 
                                                                                                ?></option>
                                            <?php //} 
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row pb-2">
                                    <label class="col-lg-3 control-label text-lg-end pt-2" for="stokproduk">Stok</label>
                                    <div class="col-lg-6">
                                        <input type="number" class="form-control form-control-sm" id="stokproduk" name="stokproduk" required>
                                    </div>
                                </div>
                                <div class="form-group row pb-2">
                                    <label class="col-lg-3 control-label text-lg-end pt-2" for="hargamodal">Harga Modal</label>
                                    <div class="col-lg-6">
                                        <input type="number" class="form-control form-control-sm" id="hargamodal" name="hargamodal" required>
                                    </div>
                                </div> -->
                                <div class="form-group row pb-2">
                                    <label class="col-lg-3 control-label text-lg-end pt-2" for="kategori">Harga Publish</label>
                                    <div class="col-sm-8">
                                        <div class="row">
                                            <div class="col-sm-4">
                                            <input type="number" placeholder="Harga Publish" class="form-control form-control-sm" id="hargapublish" name="hargapublish" required>
                                            </div>
                                            <div class="d-md-none mb-3"></div>
                                            <div class="col-lg-2 mb-3">
                                                <label class="control-label text-lg-end pt-2" for="inputDisabled">Harga Net</label>
                                            </div>
                                            <div class="d-md-none mb-3"></div>
                                            <div class="col-sm-3">
                                            <input type="number" placeholder="Harga Net" class="form-control form-control-sm" id="harganet" name="harganet" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row pb-2">
                                    <label class="col-lg-3 control-label text-lg-end pt-2" for="deskripsi">Deskripsi</label>
                                    <div class="col-lg-6">
                                        <Textarea placeholder="Deskripsi Produk" class="form-control form-control-sm" name="deskripsi" cols="100" rows="4"></Textarea>
                                    </div>
                                </div>
                                <footer class="card-footer">
                                    <div class="row justify-content-end">
                                        <div class="col-sm-9">
                                            <button class="btn btn-primary" type="submit" name="action" value="save">Simpan</button>
                                        </div>
                                </footer>
                            </form>
                        </div>
                        <br />
                    </div>

                    <table class="table table-bordered table-striped mb-0 display nowrap" id="datatables" style="width:100%">
                        <thead>
                            <tr>
                                <th width="100%">Kode Produk</th>
                                <th>Store</th>
                                <th>Kategori</th>
                                <th>Nama Produk</th>
                                <th>Stok</th>
                                <th>Harga Modal</th>
                                <th>Harga Publish</th>
                                <th>Harga Net</th>
                                <th>Deskripsi</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($produk->result_array() as $row) { ?>
                                <tr>
                                    <td><?= $row['kodeproduk'] ?></td>
                                    <td><?= $row['wilayah'] ?></td>
                                    <td><?= $row['namakategori'] ?></td>
                                    <td><?= $row['namaproduk'] ?></td>
                                    <td><?= $row['stokproduk'] ?></td>
                                    <td><?= rupiah($row['hargamodal']) ?></td>
                                    <td><?= rupiah($row['hargapublish']) ?></td>
                                    <td><?= rupiah($row['harganet']) ?></td>
                                    <td><?= $row['deskripsi'] ?></td>
                                    <td>
                                        <a class="mb-1 mt-1 me-1 modal-basic btn btn-sm btn-warning" href="#editProdukid<?= $row['idproduk'] ?>"><i class="el el-pencil"></i></a>
                                        <a class="mb-1 mt-1 me-1 modal-basic btn btn-sm btn-danger" href="#deleteProdukid<?= $row['idproduk'] ?>"><i class="el el-trash"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!-- Modal Delete -->
                    <?php foreach ($produk->result_array() as $row) { ?>

                        <div id="deleteProdukid<?= $row['idproduk'] ?>" class="modal-block modal-block-primary mfp-hide">
                            <form class="form-horizontal form-bordered" enctype="multipart/form-data" method="post" action="<?= base_url() ?>/produk/delete">
                                <section class="card">
                                    <header class="card-header">
                                        <h2 class="card-title">Hapus Data</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="modal-wrapper">
                                            <div class="modal-icon">
                                                <i class="fas fa-question-circle"></i>
                                            </div>
                                            <div class="modal-text">
                                                <input type="text" name="id" value="<?= $row['idproduk'] ?>" hidden />
                                                <p>Apakah Anda Yakin Ingin Menghapus Data <?= $row['namaproduk'] ?> ?</p>
                                            </div>
                                        </div>
                                    </div>
                                    <footer class="card-footer">
                                        <div class="row">
                                            <div class="col-md-12 text-end">
                                                <button class="btn btn-primary" type="submit" name="action" value="delete">Hapus</button>
                                                <button class="btn btn-default modal-dismiss">Batal</button>
                                            </div>
                                        </div>
                                    </footer>
                                </section>
                            </form>
                        </div>

                        <div id="editProdukid<?= $row['idproduk'] ?>" class="modal-block modal-block-primary mfp-hide">
                            <div class="modal-dialog modal-xl" role="document">
                                <div class="modal-content">
                                    <form class="form-horizontal form-bordered" enctype="multipart/form-data" method="post" action="<?= base_url() ?>/produk/edit">

                                        <div class="modal-header">
                                            <h5 class="modal-title">Edit Data</h5>
                                            <button type="button" class="close btn btn-default modal-dismiss" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="modal-wrapper">
                                                <div class="modal-text">
                                                    <div class="form-group row">
                                                        <input type="hidden" name="id" value="<?= $row['idproduk'] ?>" >
                                                        <div class="form-group row pb-4">
                                                            <label class="col-lg-3 control-label text-lg-end pt-2" for="kodeproduk">Kode Produk</label>
                                                            <div class="col-lg-3">
                                                                <input type="text" class="form-control" id="kodeproduk<?= $row['kodeproduk'] ?>" name="kodeproduk" value="<?= $row['kodeproduk'] ?>">
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <label class="checkbox-inline">
                                                                    <div class="checkbox-custom checkbox-default">
                                                                        <input type="checkbox" name="kodestatus" id="kodestatus<?= $row['kodeproduk'] ?>" onclick="ubahstatusedit(`<?= $row['kodeproduk'] ?>`)" maxlength="12">
                                                                        <label for="kodestatus">Otomatis</label>
                                                                    </div>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row pb-4">
                                                            <label class="col-lg-3 control-label text-lg-end pt-2" for="namaproduk">Nama Produk</label>
                                                            <div class="col-lg-8">
                                                                <input type="text" class="form-control" id="namaproduk" name="namaproduk" value="<?= $row['namaproduk'] ?>" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row pb-4">
                                                            <label class="col-lg-3 control-label text-lg-end pt-2" for="kategori">Kategori</label>
                                                            <div class="col-lg-8">
                                                                <select name="kodekategori" class="form-control">
                                                                    <option value="<?= $row['kodekategori'] ?>"><?= $row['namakategori'] ?></option>
                                                                    <?php $kategoridata = $this->db->query("select kodekategori,namakategori from tbkategori")->result_array();
                                                                    foreach ($kategoridata as $kategori) {
                                                                    ?>
                                                                        <option value="<?= $kategori['kodekategori'] ?>"><?= $kategori['namakategori'] ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row pb-4">
                                                            <label class="col-lg-3 control-label text-lg-end pt-2" for="unit">Unit</label>
                                                            <div class="col-lg-8">
                                                                <select name="unit" class="form-control">
                                                                    <option value="<?= $row['unit'] ?>"><?php if($row['unit']=='S'){ echo "Second"; }else{ echo "New"; } ?></option>
                                                                    <?php if($row['unit']=='S'){ echo "<option value='N'>New</option>"; }else{ echo "<option value='S'>Second</option>"; } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="form-group row pb-4">
                                                            <label class="col-lg-3 control-label text-lg-end pt-2" for="Store">Store</label>
                                                            <div class="col-lg-8">
                                                                <select name="kodestore" class="form-control">
                                                                    <option value="<?//= $row['kodestore'] ?>"><?//= $row['wilayah'] ?></option>
                                                                    <?php //$storedata = $this->db->query("select kodestore,wilayah from tbstore")->result_array();
                                                                    //foreach ($storedata as $store) {
                                                                    ?>
                                                                        <option value="<?//= $store['kodestore'] ?>"><?//= $store['wilayah'] ?></option>
                                                                    <?php //} ?>
                                                                </select>
                                                            </div>
                                                        </div> 
                                                        <div class="form-group row pb-4">
                                                            <label class="col-lg-3 control-label text-lg-end pt-2" for="stokproduk">Stok</label>
                                                            <div class="col-lg-8">
                                                                <input type="number" class="form-control" id="stokproduk" name="stokproduk" value="<?//= $row['stokproduk'] ?>" required>
                                                            </div>
                                                        </div> -->
                                                        <!-- <div class="form-group row pb-4">
                                                            <label class="col-lg-3 control-label text-lg-end pt-2" for="hargamodal">Harga Modal</label>
                                                            <div class="col-lg-8">
                                                                <input type="number" class="form-control" id="hargamodal" name="hargamodal" value="<?//= $row['hargamodal'] ?>" required>
                                                            </div>
                                                        </div> -->
                                                        <div class="form-group row pb-4">
                                                            <label class="col-lg-3 control-label text-lg-end pt-2" for="hargapublish">Harga Publish</label>
                                                            <div class="col-lg-8">
                                                                <input type="number" class="form-control" id="hargapublish" name="hargapublish" value="<?= $row['hargapublish'] ?>" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row pb-4">
                                                            <label class="col-lg-3 control-label text-lg-end pt-2" for="harganet">Harga Net</label>
                                                            <div class="col-lg-8">
                                                                <input type="number" class="form-control" id="harganet" name="harganet" value="<?= $row['harganet'] ?>" required>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row pb-4">
                                                            <label class="col-lg-3 control-label text-lg-end pt-2" for="deskripsi">Deskripsi</label>
                                                            <div class="col-lg-8">
                                                                <Textarea class="form-control" name="deskripsi" cols="100" rows="4"><?= $row['deskripsi'] ?></Textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="row">
                                                <div class="col-md-12 text-end">
                                                    <button class="btn btn-primary" type="submit" name="action" value="edit">Edit</button>
                                                    <button class="btn btn-default modal-dismiss">Batal</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </section>
        </div>
    </div>
</section>

<?php $this->load->view('layout/footer'); ?>

<script>
    function ubahstatus() {
        var checkBox = document.getElementById("kodestatus");

        if (checkBox.checked == true) {
            document.getElementById("kodeproduk").readOnly = true;
            document.getElementById("kodeproduk").required = false;
        } else {
            document.getElementById("kodeproduk").readOnly = false;
            document.getElementById("kodeproduk").required = true;
        }
    }

    function ubahstatusedit(id) {

        var checkBox = document.getElementById("kodestatus" + id);

        if (checkBox.checked == true) {
            document.getElementById("kodeproduk" + id).readOnly = true;
            document.getElementById("kodeproduk" + id).required = false;
        } else {
            document.getElementById("kodeproduk" + id).readOnly = false;
            document.getElementById("kodeproduk" + id).required = true;
        }

    }
</script>


</body>

</html>