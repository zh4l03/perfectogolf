<?php
$dataHeader['title'] = "Master | Data Pegawai";
$this->load->view('layout/heading', $dataHeader);

$this->load->view('layout/header');

$this->load->view('layout/sidebar');
?>

<section role="main" class="content-body">
    <header class="page-header">
        <h2>Data Pegawai</h2>
        <div class="right-wrapper text-end">
            <ol class="breadcrumbs">
                <li>
                    <a href="index.html">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li><span>Table</span></li>
                <li><span>Pegawai</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
        </div>
    </header>
    <div class="row">
        <div class="col">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Data pegawai</h2>
                </header>
                <div class="card-body">

                    <div class="float-right">
                        <button type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" class="btn-sm mb-1 mt-1 me-1 btn btn-primary btn pull-right">
                            <i class="el el-plus"></i>&nbsp; Tambah pegawai
                        </button>
                    </div> <br />
                    <hr />

                    <div class="collapse" id="collapseExample">
                        <div class="card card-body">
                            <form class="form-horizontal form-bordered" enctype="multipart/form-data" method="post" action="<?= base_url() ?>/pegawai/add">
                                <div class="form-group row pb-4">
                                    <label class="col-lg-3 control-label text-lg-end pt-2" for="namapegawai">Nama pegawai</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" id="namapegawai" name="namapegawai" required>
                                    </div>
                                </div>
                                <div class="form-group row pb-4">
                                    <label class="col-lg-3 control-label text-lg-end pt-2" for="Store">Departement</label>
                                    <div class="col-lg-6">
                                        <select name="iddept" class="form-control">
                                            <option>Pilih Departement</option>
                                            <?php $iddeptdata = $this->db->query("select iddept,namadept from tbdepartment")->result_array();
                                            foreach ($iddeptdata as $iddept) {
                                            ?>
                                                <option value="<?= $iddept['iddept'] ?>"><?= $iddept['namadept'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row pb-4">
                                    <label class="col-lg-3 control-label text-lg-end pt-2" for="kategori">Jabatan</label>
                                    <div class="col-lg-6">
                                        <select name="idjabatan" class="form-control">
                                            <option>Pilih Jabatan</option>
                                            <?php $jabatandata = $this->db->query("select idjabatan,namajabatan from tbjabatan")->result_array();
                                            foreach ($jabatandata as $jabatan) {
                                            ?>
                                                <option value="<?= $jabatan['idjabatan'] ?>"><?= $jabatan['namajabatan'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row pb-4">
                                    <label class="col-lg-3 control-label text-lg-end pt-2" for="Store">Store</label>
                                    <div class="col-lg-6">
                                        <select name="kodestore" class="form-control">
                                            <option>Pilih Store</option>
                                            <?php $storedata = $this->db->query("select kodestore,wilayah from tbstore")->result_array();
                                            foreach ($storedata as $store) {
                                            ?>
                                                <option value="<?= $store['kodestore'] ?>"><?= $store['wilayah'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row pb-4">
                                    <label class="col-lg-3 control-label text-lg-end pt-2" for="username">Username</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" id="username" name="username" required>
                                        <span id='messageusername'></span>
                                    </div>
                                </div>
                                <div class="form-group row pb-4">
                                    <label class="col-lg-3 control-label text-lg-end pt-2" for="pwd">Password</label>
                                    <div class="col-lg-6">
                                        <input type="password" class="form-control" id="pwd" name="pwd" value="perfecto" readonly>
                                        <span id='message'>Default Password : perfecto</span>
                                    </div>
                                </div>
                                <div class="form-group row pb-4" style="display:none;">
                                    <label class="col-lg-3 control-label text-lg-end pt-2" for="pwd">Konfirmasi</label>
                                    <div class="col-lg-6">
                                        <input type="password" class="form-control" id="kpwd" name="kpwd" value="perfecto" readonly>
                                        <span id='message'></span>
                                    </div>
                                </div>

                                <footer class="card-footer">
                                    <div class="row justify-content-end">
                                        <div class="col-sm-9">
                                            <button class="btn btn-primary" type="submit" name="action" value="save" id="action">Simpan</button>
                                        </div>
                                </footer>
                            </form>
                        </div>
                        <br />
                    </div>

                    <table class="table table-bordered table-striped mb-0 display nowrap" id="datatables" style="width:100%">
                        <thead>
                            <tr>
                                <th>Nama Pegawai</th>
                                <th>Jabatan</th>
                                <th>Departemnt</th>
                                <th>Store</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($pegawai->result_array() as $row) { ?>
                                <tr>
                                    <td><?= $row['namapegawai'] ?></td>
                                    <td><?= $row['namajabatan'] ?></td>
                                    <td><?= $row['namadept'] ?></td>
                                    <td><?= $row['wilayah'] ?></td>
                                    <td>
                                        <a class="mb-1 mt-1 me-1 modal-basic btn btn-sm btn-warning" href="#editpegawaiid<?= $row['idpegawai'] ?>"><i class="el el-pencil"></i></a>
                                        <a class="mb-1 mt-1 me-1 modal-basic btn btn-sm btn-danger" href="#deletepegawaiid<?= $row['idpegawai'] ?>"><i class="el el-trash"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!-- Modal Delete -->
                    <?php foreach ($pegawai->result_array() as $row) { ?>

                        <div id="deletepegawaiid<?= $row['idpegawai'] ?>" class="modal-block modal-block-primary mfp-hide">
                            <form class="form-horizontal form-bordered" enctype="multipart/form-data" method="post" action="<?= base_url() ?>/pegawai/delete">
                                <section class="card">
                                    <header class="card-header">
                                        <h2 class="card-title">Hapus Data</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="modal-wrapper">
                                            <div class="modal-icon">
                                                <i class="fas fa-question-circle"></i>
                                            </div>
                                            <div class="modal-text">
                                                <input type="text" name="id" value="<?= $row['idpegawai'] ?>" hidden />
                                                <p>Apakah Anda Yakin Ingin Menghapus Data <?= $row['namapegawai'] ?> ?</p>
                                            </div>
                                        </div>
                                    </div>
                                    <footer class="card-footer">
                                        <div class="row">
                                            <div class="col-md-12 text-end">
                                                <button class="btn btn-primary" type="submit" name="action" value="delete">Hapus</button>
                                                <button class="btn btn-default modal-dismiss">Batal</button>
                                            </div>
                                        </div>
                                    </footer>
                                </section>
                            </form>
                        </div>

                        <div id="editpegawaiid<?= $row['idpegawai'] ?>" class="modal-block modal-block-primary mfp-hide">
                            <div class="modal-dialog modal-xl" role="document">
                                <div class="modal-content">
                                    <form class="form-horizontal form-bordered" enctype="multipart/form-data" method="post" action="<?= base_url() ?>/pegawai/edit">

                                        <div class="modal-header">
                                            <h5 class="modal-title">Edit Data</h5>
                                            <button type="button" class="close btn btn-default modal-dismiss" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="modal-wrapper">
                                                <div class="modal-text">
                                                    <input type="text" name="id" value="<?= $row['idpegawai'] ?>" hidden />
                                                    <div class="form-group row">
                                                        <div class="form-group row pb-4">
                                                            <label class="col-lg-3 control-label text-lg-end pt-2" for="namapegawai">Nama pegawai</label>
                                                            <div class="col-lg-6">
                                                                <input type="text" class="form-control" id="namapegawai" name="namapegawai" value="<?= $row['namapegawai'] ?>" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row pb-4">
                                                            <label class="col-lg-3 control-label text-lg-end pt-2" for="Store">Departement</label>
                                                            <div class="col-lg-6">
                                                                <select name="iddept" class="form-control">
                                                                    <option value="<?= $row['iddept'] ?>"><?= $row['namadept'] ?></option>
                                                                    <?php $storedata = $this->db->query("select iddept,namadept from tbdepartment")->result_array();
                                                                    foreach ($storedata as $store) {
                                                                    ?>
                                                                        <option value="<?= $store['iddept'] ?>"><?= $store['namadept'] ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row pb-4">
                                                            <label class="col-lg-3 control-label text-lg-end pt-2" for="kategori">Jabatan</label>
                                                            <div class="col-lg-6">
                                                                <select name="idjabatan" class="form-control">
                                                                    <option value="<?= $row['idjabatan'] ?>?>"><?= $row['namajabatan'] ?></option>
                                                                    <?php $jabatandata = $this->db->query("select idjabatan,namajabatan from tbjabatan")->result_array();
                                                                    foreach ($jabatandata as $jabatan) {
                                                                    ?>
                                                                        <option value="<?= $jabatan['idjabatan'] ?>"><?= $jabatan['namajabatan'] ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row pb-4">
                                                            <label class="col-lg-3 control-label text-lg-end pt-2" for="Store">Store</label>
                                                            <div class="col-lg-6">
                                                                <select name="kodestore" class="form-control">
                                                                    <option value="<?= $row['kodestore'] ?>"><?= $row['wilayah'] ?></option>
                                                                    <?php $storedata = $this->db->query("select kodestore,wilayah from tbstore")->result_array();
                                                                    foreach ($storedata as $store) {
                                                                    ?>
                                                                        <option value="<?= $store['kodestore'] ?>"><?= $store['wilayah'] ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row pb-4">
                                                            <label class="col-lg-3 control-label text-lg-end pt-2" for="username">Username</label>
                                                            <div class="col-lg-6">
                                                                <input type="text" class="form-control" id="username<?= $row['idpegawai'] ?>" name="username" value="<?= $row['username'] ?>" required>
                                                                <input type="hidden" class="form-control" id="usernamelama<?= $row['idpegawai'] ?>" name="usernamelama" value="<?= $row['username'] ?>" required>
                                                                <span id='messageusername<?= $row['idpegawai'] ?>'></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row pb-4">
                                                            <label class="col-lg-3 control-label text-lg-end pt-2" for="pwd">Password</label>
                                                            <div class="col-lg-3">
                                                                <input type="password" class="form-control" id="pwd<?= $row['idpegawai'] ?>" name="pwd" value="<?= $row['pwd'] ?>" readonly>
                                                                <input type="hidden" class="form-control" id="pwdlama<?= $row['idpegawai'] ?>" name="pwdlama" value="<?= $row['pwd'] ?>" readonly>
                                                            </div>
                                                            <div class="col-lg-3" id="resetpass<?= $row['idpegawai'] ?>">
                                                                <input type="button" class="btn btn-warning" value="Reset" onclick="resetpassword('<?= $row['idpegawai'] ?>')">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row pb-4" style="display:none;">
                                                            <label class="col-lg-3 control-label text-lg-end pt-2" for="pwd">Konfirmasi</label>
                                                            <div class="col-lg-6">
                                                                <input type="password" class="form-control" id="kpwd<?= $row['idpegawai'] ?>" name="kpwd" value="<?= $row['pwd'] ?>" required>
                                                                <span id='message<?= $row['idpegawai'] ?>'></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="row">
                                                <div class="col-md-12 text-end">
                                                    <button class="btn btn-primary" type="submit" name="action" value="edit" id="action<?= $row['idpegawai'] ?>">Edit</button>
                                                    <button class="btn btn-default modal-dismiss">Batal</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </section>
        </div>
    </div>
</section>

<?php $this->load->view('layout/footer'); ?>

<script>
    function resetpassword(id) {
        document.getElementById('resetpass'+id).innerHTML = " <input type='button' class='btn btn-warning' value='Unreset' onclick='passwordunreset(`"+id+"`)'>";
        document.getElementById('pwd'+id).value = "perfecto";
        document.getElementById('kpwd'+id).value = "perfecto";
    }

    function passwordunreset(id) {
        document.getElementById('resetpass'+id).innerHTML = " <input type='button' class='btn btn-warning' value='Reset' onclick='resetpassword(`"+id+"`)'>";
        document.getElementById('pwd'+id).value = document.getElementById('pwdlama'+id).value;
        document.getElementById('kpwd'+id).value = document.getElementById('pwdlama'+id).value;
    }

    let cekusername = '1';
    $('#pwd, #kpwd').on('keyup', function() {
        if ($('#pwd').val() == $('#kpwd').val()) {
            $('#message').html('Matching').css('color', 'green');
            cekall();
        } else {
            $('#message').html('Not Matching').css('color', 'red');
            cekall();
        }
    });

    $('#username').on('keyup', function() {
        // $('#messageusername').html('Not Matching').css('color', 'red');
        $.ajax({
            url: "<?php echo base_url(); ?>pegawai/cek_username",
            method: "POST",
            data: {
                username: document.getElementById("username").value,
            },
            dataType: 'json',
            // contentType: "application/json",
            success: function(data) {
                if (data.status == '1') {
                    cekusername = '1';
                    $('#messageusername').html('').css('color', 'red');
                } else {
                    cekusername = '0';
                    $('#messageusername').html("Username Sudah Di Pakai").css('color', 'red');
                }
                cekall();
            },
            error: function(errMsg) {
                alert(JSON.stringify(errMsg));
            }
        });



    });

    function cekall() {
        if (($('#pwd').val() == $('#kpwd').val()) && cekusername == '1') {
            document.getElementById("action").disabled = false;
        } else {
            document.getElementById("action").disabled = true;
        }
    }


    <?php foreach ($pegawai->result_array() as $row) { ?>
        let cekusername<?= $row['idpegawai'] ?> = '1';

        $('#username<?= $row['idpegawai'] ?>').on('keyup', function() {
            // $('#messageusername').html('Not Matching').css('color', 'red');
            if (document.getElementById("username<?= $row['idpegawai'] ?>").value == document.getElementById("usernamelama<?= $row['idpegawai'] ?>").value) {
                cekusername<?= $row['idpegawai'] ?> = '1';
                $('#messageusername<?= $row['idpegawai'] ?>').html('').css('color', 'red');
            } else {
                $.ajax({
                    url: "<?php echo base_url(); ?>pegawai/cek_username",
                    method: "POST",
                    data: {
                        username: document.getElementById("username<?= $row['idpegawai'] ?>").value,
                    },
                    dataType: 'json',
                    // contentType: "application/json",
                    success: function(data) {
                        if (data.status == '1') {
                            cekusername<?= $row['idpegawai'] ?> = '1';
                            $('#messageusername<?= $row['idpegawai'] ?>').html('').css('color', 'red');
                        } else {
                            cekusername<?= $row['idpegawai'] ?> = '0';
                            $('#messageusername<?= $row['idpegawai'] ?>').html("Username Sudah Di Pakai").css('color', 'red');
                        }
                        cekall<?= $row['idpegawai'] ?>();
                    },
                    error: function(errMsg) {
                        alert(JSON.stringify(errMsg));
                    }
                });
            }



        });

        $('#pwd<?= $row['idpegawai'] ?>, #kpwd<?= $row['idpegawai'] ?>').on('keyup', function() {
            if ($('#pwd<?= $row['idpegawai'] ?>').val() == $('#kpwd<?= $row['idpegawai'] ?>').val()) {
                $('#message<?= $row['idpegawai'] ?>').html('Matching').css('color', 'green');
                cekall<?= $row['idpegawai'] ?>();
            } else {
                $('#message<?= $row['idpegawai'] ?>').html('Not Matching').css('color', 'red');
                cekall<?= $row['idpegawai'] ?>();
            }
        });

        function cekall<?= $row['idpegawai'] ?>() {
            if (($('#pwd<?= $row['idpegawai'] ?>').val() == $('#kpwd<?= $row['idpegawai'] ?>').val()) && cekusername<?= $row['idpegawai'] ?> == '1') {
                document.getElementById("action<?= $row['idpegawai'] ?>").disabled = false;
            } else {
                document.getElementById("action<?= $row['idpegawai'] ?>").disabled = true;
            }
        }


    <?php } ?>
</script>


</body>

</html>