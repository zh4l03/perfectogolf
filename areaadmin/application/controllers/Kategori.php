<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kategori extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data["kategori"] = $this->model_app->view('tbkategori');
		$this->load->view('kategori/index', $data);
	}

	public function add()
	{
		if ($this->input->post('action') == 'save') {

			$namakategori = $this->input->post('namakategori');
			$kodekategori = $this->input->post('kodekategori');

			$data = array(
				'namakategori' => $namakategori,
				'kodekategori' => $kodekategori,
				'logtgl' => date("Y-m-d h:i:sa"),
				'loguser' => $this->session->idpegawai,
			);
			$cek = $this->db->query("select namakategori from tbkategori where kodekategori='$kodekategori'")->num_rows();
			if ($cek > 0) {
				$this->session->set_flashdata('error', 'Kode Kategori Sudah DI Pakai');
				redirect('/kategori/add');
			} else {
				$this->db->insert('tbkategori', $data);
				$this->session->set_flashdata('success', 'Kategori Berhasil Di Tambahkan');
				redirect('/kategori/index');
			}
		}

		redirect('/kategori/index');
	}

	public function edit()
	{
		if ($this->input->post('action') == 'edit') {

			$namakategori = $this->input->post('namakategori');
			$kodekategori = $this->input->post('kodekategori');
			$data = array(
				'namakategori' => $namakategori,
				'kodekategori' => $kodekategori,
				'logtgl' => date("Y-m-d h:i:sa"),
				'loguser' => $this->session->idpegawai,
			);
			if ($kodekategori != $this->input->post('kodelama')) {
				$cek = $this->db->query("select namakategori from tbkategori where kodekategori='$kodekategori'")->num_rows();
				if ($cek > 0) {
					$this->session->set_flashdata('error', 'Kode Kategori Sudah DI Pakai');
					redirect("/kategori/edit/" . $this->input->post('id'));
				}
			}
			$this->db->update('tbkategori', $data, array('kodekategori' => $this->input->post('id')));
			$this->session->set_flashdata('success', 'kategori Berhasil Di Ubah');
			redirect('/kategori/index');
		}
		redirect('/kategori/index');
	}

	public function delete()
	{
		if ($this->input->post('action') == 'delete') {
			$inputkode = $this->db->query("select kodekategori from tbproduk where kodekategori='" . $this->input->post('id') . "'")->num_rows();
			if ($inputkode > 0) {
				$this->session->set_flashdata('warning', 'Data Tidak Boleh Di Hapus');
			} else {
				$this->db->delete('tbkategori', array('kodekategori' => $this->input->post('id')));
				$this->session->set_flashdata('success', 'kategori Berhasil Di Hapus');
			}
			redirect('/kategori/index');
		}
		redirect('/kategori/index');
	}
}
