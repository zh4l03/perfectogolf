<?php
defined('BASEPATH') or exit('No direct script access allowed');

class produk extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data["produk"] = $this->db->query("select a.idproduk, a.unit, a.kodeproduk,a.kodekategori,a.kodestore,a.namaproduk,a.deskripsi,a.hargamodal,a.harganet,a.hargapublish,a.stokproduk, b.namakategori,c.wilayah from tbproduk a join tbkategori b on a.kodekategori=b.kodekategori join tbstore c on a.kodestore=c.kodestore");
		$this->load->view('produk/index', $data);
	}

	public function add()
	{
		if ($this->input->post('action') == 'save') {

			$kodeproduk = $this->input->post('kodeproduk');
			$kodekategori = $this->input->post('kodekategori');
			$unit = $this->input->post('unit');
			$kodestore = "PUSAT";
			$namaproduk = $this->input->post('namaproduk');
			//$hargamodal = $this->input->post('hargamodal');
			$hargapublish = $this->input->post('hargapublish');
			$harganet = $this->input->post('harganet');
			$deskripsi = $this->input->post('deskripsi');
			//$stokproduk = $this->input->post('stokproduk');
			

			$data = array(
				'kodekategori' => $kodekategori,
				'kodestore' => $kodestore,
				'namaproduk' => $namaproduk,
				'unit' => $unit,
				//'stokproduk' => $stokproduk,
				//'hargamodal' => $hargamodal,
				'hargapublish' => $hargapublish,
				'harganet' => $harganet,
				'deskripsi' => $deskripsi,
				'logtgl' => date("Y-m-d h:i:sa"),
				'loguser' => $this->session->idpegawai,
			);

			if ($this->input->post('kodestatus') == null) {
				$data['kodeproduk'] = $kodeproduk;
				
			} else {
				$kode = $kodekategori." - ".$unit;
				$cekKode = $this->db->query("select number from tbkodesetting where kodekategori='" . $kodekategori . "' && unit='" . $unit . "'");
				if ($cekKode->num_rows() > 0) {
					$nextkode =  sprintf("%04d", (int)substr($cekKode->row_array()['number'], -4) + 1);
					$data['kodeproduk'] = $kode . "" . $nextkode;

					$savekode = array(
						'number'	=> $nextkode,
					);
					$where = array(
						'kodekategori' => $kodekategori,
						'unit' => $unit
					);
					$this->db->update('tbkodesetting', $savekode, $where);
				} else {
					$data['kodeproduk'] = $kode . "0001";
					$savekode = array(
						'kodekategori' => $kodekategori,
						'unit' => $unit,
						'number'	=> '1'
					);
					$this->db->insert('tbkodesetting', $savekode);
				}
			}

			$this->db->insert('tbproduk', $data);
			$this->session->set_flashdata('success', 'produk Berhasil Di Tambahkan');
			redirect('/produk/index');
		}
		redirect('/produk/index');
	}

	public function edit()
	{
		if ($this->input->post('action') == 'edit') {

			$kodeproduk = $this->input->post('kodeproduk');
			$kodekategori = $this->input->post('kodekategori');
			$unit = $this->input->post('unit');
			$kodestore = "PUSAT";
			$namaproduk = $this->input->post('namaproduk');
			//$hargamodal = $this->input->post('hargamodal');
			$hargapublish = $this->input->post('hargapublish');
			$harganet = $this->input->post('harganet');
			$deskripsi = $this->input->post('deskripsi');
			//$stokproduk = $this->input->post('stokproduk');

			$data = array(
				'kodekategori' => $kodekategori,
				'kodestore' => $kodestore,
				'namaproduk' => $namaproduk,
				'unit' => $unit,
				//'stokproduk' => $stokproduk,
				//'hargamodal' => $hargamodal,
				'hargapublish' => $hargapublish,
				'harganet' => $harganet,
				'deskripsi' => $deskripsi,
				'logtgl' => date("Y-m-d h:i:sa"),
				'loguser' => $this->session->idpegawai,
			);
			if ($this->input->post('kodestatus') == null) {
				$data['kodeproduk'] = $kodeproduk;
			} else {
				$validasi = $this->db->query("select kodekategori,unit from tbproduk where kodeproduk='" . $kodeproduk . "'")->row_array();
				if ($validasi['kodekategori'] != $kodekategori || $validasi['unit'] != $unit) {
					$cekKode = $this->db->query("select number from tbkodesetting where kodekategori='" . $kodekategori . "' and unit='" . $unit . "'");
					$kode = $kodekategori . " - " . $unit;
					if ($cekKode->num_rows() > 0) {
						$nextkode =  sprintf("%04d", (int)substr($cekKode->row_array()['number'], -4) + 1);
						$data['kodeproduk'] = $kode . "" . $nextkode;

						$savekode = array(
							'number'	=> $nextkode,
						);
						$where = array(
							'kodekategori' => $kodekategori,
							'unit' => $unit,
						);
						$this->db->update('tbkodesetting', $savekode, $where);
					} else {
						$data['kodeproduk'] = $kode . "0001";
						$savekode = array(
							'kodekategori' => $kodekategori,
							'unit' => $unit,
							'number'	=> '1'
						);
						$this->db->insert('tbkodesetting', $savekode);
					}
				}
			}
			$this->db->update('tbproduk', $data, array('idproduk' => $this->input->post('id')));
			$this->session->set_flashdata('success', 'produk Berhasil Di Ubah');
			redirect('/produk/index');
		}

		redirect('/produk/index');
	}

	public function delete()
	{
		if ($this->input->post('action') == 'delete') {
			$this->db->delete('tbproduk', array('idproduk' => $this->input->post('id')));
			$this->session->set_flashdata('success', 'produk Berhasil Di Hapus');
			redirect('/produk/index');
		}
		redirect('/produk/index');
	}
}
