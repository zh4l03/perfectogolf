<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Template_crud extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data["produk"] = $this->model_app->view('produk');
		$this->load->view('template_crud/index', $data);
	}

	public function add()
	{
		if ($this->input->post('action')=='save') {

			$namaProduk = $this->input->post('namaProduk');
			$hargaJual = $this->input->post('hargaJual');
			$hargaBeli = $this->input->post('hargaBeli');
			$keterangan = $this->input->post('keterangan');

			$data = array(
				'namaProduk' => $namaProduk,
				'hargaJual' => $hargaJual,
				'hargaBeli' => $hargaBeli,
				'keterangan' => $keterangan,
			);

			$config['upload_path']          = '../asset/produk/';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['max_size']             = 5000;
			$config['encrypt_name'] = false;
			$filename = $namaProduk . "_" . date("Y-m-d") . ".jpg";
			$config['file_name'] = $filename;
			// $config['max_width']            = 1024;
			// $config['max_height']           = 768;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('gambar')) {
				$error = array('error' => $this->upload->display_errors());
				var_dump($error);
			} else {
				$data['gambar'] = $filename;
				$this->db->insert('produk', $data);
				$this->session->set_flashdata('success', 'Produk Berhasil Di Tambahkan');
				redirect('/template_crud/index');
			}
		}

		$data["produk"] = $this->model_app->view('produk');
		$this->load->view('template_crud/add', $data);
	}

	public function edit()
	{
		if ($this->input->post('action')=='edit') {

			$namaProduk = $this->input->post('namaProduk');
			$hargaJual = $this->input->post('hargaJual');
			$hargaBeli = $this->input->post('hargaBeli');
			$keterangan = $this->input->post('keterangan');

			$data = array(
				'namaProduk' => $namaProduk,
				'hargaJual' => $hargaJual,
				'hargaBeli' => $hargaBeli,
				'keterangan' => $keterangan,
			);

			$config['upload_path']          = '../asset/produk/';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['max_size']             = 5000;
			$config['encrypt_name'] = false;
			$filename = $namaProduk . "_" . date("Y-m-d") . ".jpg";
			$config['file_name'] = $filename;
			// $config['max_width']            = 1024;
			// $config['max_height']           = 768;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('gambar')) {
				$this->db->update('produk', $data, array('id' => $this->input->post('id')));
			} else {
				$data['gambar'] = $filename;
				$this->db->update('produk', $data, array('id' => $this->input->post('id')));
				$gambar_lama = $this->input->post('gambar_lama');
				unlink("../asset/produk/$gambar_lama");
			}
			$this->session->set_flashdata('success', 'Produk Berhasil Di Ubah');
			redirect('/template_crud/index');
		}
		if($this->uri->segment(3)==''){
			redirect('/template_crud/index');
		}

		$data["produk"] = $this->db->query("select id,namaProduk,hargaBeli,hargaJual,keterangan,gambar from produk where id='".$this->uri->segment(3)."'")->row_array();
		$this->load->view('template_crud/edit', $data);
	}

	public function delete()
	{
		if ($this->input->post('action')=='delete') {
			$gambar = $this->input->post('gambar');
			unlink("../asset/produk/$gambar");
			$this->db->delete('produk',array('id' => $this->input->post('id')));
			$this->session->set_flashdata('success', 'Produk Berhasil Di Hapus');
			redirect('/template_crud/index');
		}
		redirect('/template_crud/index');
		
	}
}
