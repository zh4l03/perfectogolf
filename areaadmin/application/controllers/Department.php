<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Department extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data["department"] = $this->model_app->view('tbdepartment');
		$this->load->view('department/index', $data);
	}

	public function add()
	{
		if ($this->input->post('action') == 'save') {

			$namadepartment = $this->input->post('namadepartment');

			$data = array(
				'namadept' => $namadepartment,
				'logtgl' => date("Y-m-d h:i:sa"),
				'loguser' => $this->session->idpegawai,
			);

			$this->db->insert('tbdepartment', $data);
			$this->session->set_flashdata('success', 'Department Baru Berhasil Di Tambahkan');
			redirect('/department/index');
		}

		redirect('/department/index');
	}

	public function edit()
	{
		if ($this->input->post('action') == 'edit') {

			$namadepartment = $this->input->post('namadepartment');

			$data = array(
				'namadept' => $namadepartment,
				'logtgl' => date("Y-m-d h:i:sa"),
				'loguser' => $this->session->idpegawai,
			);
			
			$this->db->update('tbdepartment', $data, array('iddept' => $this->input->post('id')));
			$this->session->set_flashdata('success', 'department Berhasil Di Ubah');
			redirect('/department/index');
		}

		redirect('/department/index');
	}

	public function delete()
	{
		if ($this->input->post('action') == 'delete') {
			$inputkode = $this->db->query("select iddept from tbpegawai where iddept='" . $this->input->post('id') . "'")->num_rows();
			if ($inputkode > 0) {
				$this->session->set_flashdata('warning', 'Data Tidak Boleh Di Hapus');
			} else {
				$this->db->delete('tbdepartment', array('iddept' => $this->input->post('id')));
				$this->session->set_flashdata('success', 'department Berhasil Di Hapus');
			}
			redirect('/department/index');
		}
		redirect('/department/index');
	}
}
