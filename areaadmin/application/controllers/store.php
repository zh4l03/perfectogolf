<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Store extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data["store"] = $this->model_app->view('tbstore');
		$this->load->view('store/index', $data);
	}

	public function add()
	{
		if ($this->input->post('action') == 'save') {

			$wilayah = $this->input->post('wilayah');
			$kodestore = $this->input->post('kodestore');
			$alamat = $this->input->post('alamat');

			$data = array(
				'wilayah' => $wilayah,
				'kodestore' => $kodestore,
				'alamat'	=> $alamat,
				'logtgl' => date("Y-m-d h:i:sa"),
				'loguser' => $this->session->idpegawai,
			);
			$cek = $this->db->query("select wilayah from tbstore where kodestore='$kodestore'")->num_rows();
			if ($cek > 0) {
				$this->session->set_flashdata('error', 'Kode store Sudah DI Pakai');
				redirect('/store/add');
			} else {
				$this->db->insert('tbstore', $data);
				$this->session->set_flashdata('success', 'store Berhasil Di Tambahkan');
				redirect('/store/index');
			}
		}

		redirect('/store/index');
	}

	public function edit()
	{
		if ($this->input->post('action') == 'edit') {

			$wilayah = $this->input->post('wilayah');
			$kodestore = $this->input->post('kodestore');
			$alamat = $this->input->post('alamat');

			$data = array(
				'wilayah' => $wilayah,
				'kodestore' => $kodestore,
				'alamat'	=> $alamat,
				'logtgl' => date("Y-m-d h:i:sa"),
				'loguser' => $this->session->idpegawai,
			);
			if ($kodestore != $this->input->post('kodelama')) {
				$cek = $this->db->query("select wilayah from tbstore where kodestore='$kodestore'")->num_rows();
				if ($cek > 0) {
					$this->session->set_flashdata('error', 'Kode store Sudah Di Pakai');
					redirect("/store/edit/" . $this->input->post('id'));
				}
			}
			$this->db->update('tbstore', $data, array('kodestore' => $this->input->post('id')));
			$this->session->set_flashdata('success', 'store Berhasil Di Ubah');
			redirect('/store/index');
		}
		redirect('/store/index');
	}

	public function delete()
	{
		if ($this->input->post('action') == 'delete') {
			
			$inputkode = $this->db->query("select kodestore from tbproduk where kodestore='" . $this->input->post('id') . "'")->num_rows();
			if ($inputkode > 0) {
				$this->session->set_flashdata('warning', 'Data Tidak Boleh Di Hapus');
			} else {
				$this->db->delete('tbstore', array('kodestore' => $this->input->post('id')));
				$this->session->set_flashdata('success', 'store Berhasil Di Hapus');
			}
			redirect('/store/index');
		}
		redirect('/store/index');
	}
}
