<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pegawai extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data["pegawai"] = $this->db->query("Select e.username,e.pwd,a.idpegawai,a.idjabatan,a.iddept,a.kodestore,a.namapegawai,b.namajabatan,c.namadept,d.wilayah from tbpegawai a join tbjabatan b on a.idjabatan=b.idjabatan join tbdepartment c on a.iddept=c.iddept join tbstore d on a.kodestore=d.kodestore join tbuser e on a.idpegawai=e.idpegawai");
		$this->load->view('pegawai/index', $data);
	}

	public function add()
	{
		if ($this->input->post('action') == 'save') {

			$namapegawai = $this->input->post('namapegawai');
			$idjabatan = $this->input->post('idjabatan');
			$iddept = $this->input->post('iddept');
			$kodestore = $this->input->post('kodestore');
			$username = $this->input->post('username');
			$pwd = $this->input->post('pwd');
			$level = "";
			
			$data = array(
				'namapegawai' => $namapegawai,
				'idjabatan' => $idjabatan,
				'iddept' => $iddept,
				'kodestore' => $kodestore,
				'logtgl' => date("Y-m-d h:i:sa"),
				'loguser' => $this->session->idpegawai,
			);

			$this->db->insert('tbpegawai', $data);
			$idpegawai = $this->db->insert_id();
			$datauser = array(
				'username' => $username,
				'pwd' => $pwd,
				'idpegawai' => $idpegawai,
				'level' => $level,
			);
			$this->db->insert('tbuser', $datauser);
			$this->session->set_flashdata('success', 'pegawai Baru Berhasil Di Tambahkan');
			redirect('/pegawai/index');
		}

		redirect('/pegawai/index');
	}

	public function edit()
	{
		if ($this->input->post('action') == 'edit') {

			$namapegawai = $this->input->post('namapegawai');
			$idjabatan = $this->input->post('idjabatan');
			$iddept = $this->input->post('iddept');
			$kodestore = $this->input->post('kodestore');
			$username = $this->input->post('username');
			$pwd = $this->input->post('pwd');
			$level = "";

			$data = array(
				'namapegawai' => $namapegawai,
				'idjabatan' => $idjabatan,
				'iddept' => $iddept,
				'kodestore' => $kodestore,
				'logtgl' => date("Y-m-d h:i:sa"),
				'loguser' => $this->session->idpegawai,
			);
			$this->db->update('tbpegawai', $data, array('idpegawai' => $this->input->post('id')));
			$datauser = array(
				'username' => $username,
				'pwd' => $pwd,
				'level' => $level,
			);
			$this->db->update('tbuser', $datauser, array('idpegawai' => $this->input->post('id')));
			$this->session->set_flashdata('success', 'pegawai Berhasil Di Ubah');
			redirect('/pegawai/index');
		}

		redirect('/pegawai/index');
	}

	public function delete()
	{
		if ($this->input->post('action') == 'delete') {
			// $inputkode = $this->db->query("select iddept from tbpegawai where iddept='" . $this->input->post('id') . "'")->num_rows();
			// if ($inputkode > 0) {
			// 	$this->session->set_flashdata('warning', 'Data Tidak Boleh Di Hapus');
			// } else {
				$this->db->delete('tbpegawai', array('idpegawai' => $this->input->post('id')));
				$this->session->set_flashdata('success', 'pegawai Berhasil Di Hapus');
			// }
			redirect('/pegawai/index');
		}
		redirect('/pegawai/index');
	}

	public function cek_username(){
		// $username = "budi";
		$username = $this->input->post('username');
		$cek = $this->db->query("select username from tbuser where username='".$username."'")->num_rows();
		if($cek>0){
			$output = array("status"=>'0');
		}else{
			$output = array("status"=>'1');
		}
		
		echo json_encode($output);
	}
}
