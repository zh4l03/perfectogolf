<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jabatan extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data["jabatan"] = $this->model_app->view('tbjabatan');
		$this->load->view('jabatan/index', $data);
	}

	public function add()
	{
		if ($this->input->post('action') == 'save') {

			$namajabatan = $this->input->post('namajabatan');

			$data = array(
				'namajabatan' => $namajabatan,
				'logtgl' => date("Y-m-d h:i:sa"),
				'loguser' => $this->session->idpegawai,
			);

			$this->db->insert('tbjabatan', $data);
			$this->session->set_flashdata('success', 'jabatan Baru Berhasil Di Tambahkan');
			redirect('/jabatan/index');
		}

		redirect('/jabatan/index');
	}

	public function edit()
	{
		if ($this->input->post('action') == 'edit') {

			$namajabatan = $this->input->post('namajabatan');

			$data = array(
				'namajabatan' => $namajabatan,
				'logtgl' => date("Y-m-d h:i:sa"),
				'loguser' => $this->session->idpegawai,
			);
			
			$this->db->update('tbjabatan', $data, array('idjabatan' => $this->input->post('id')));
			$this->session->set_flashdata('success', 'jabatan Berhasil Di Ubah');
			redirect('/jabatan/index');
		}

		redirect('/jabatan/index');
	}

	public function delete()
	{
		if ($this->input->post('action') == 'delete') {
			$inputkode = $this->db->query("select idjabatan from tbpegawai where idjabatan='" . $this->input->post('id') . "'")->num_rows();
			if ($inputkode > 0) {
				$this->session->set_flashdata('warning', 'Data Tidak Boleh Di Hapus');
			} else {
				$this->db->delete('tbjabatan', array('idjabatan' => $this->input->post('id')));
				$this->session->set_flashdata('success', 'jabatan Berhasil Di Hapus');
			}
			redirect('/jabatan/index');
		}
		redirect('/jabatan/index');
	}
}
