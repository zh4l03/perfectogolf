/*
 Navicat Premium Data Transfer

 Source Server         : LokalDB
 Source Server Type    : MariaDB
 Source Server Version : 100703
 Source Host           : localhost:3306
 Source Schema         : perfectogolfdb

 Target Server Type    : MariaDB
 Target Server Version : 100703
 File Encoding         : 65001

 Date: 18/03/2022 23:14:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbaksi
-- ----------------------------
DROP TABLE IF EXISTS `tbaksi`;
CREATE TABLE `tbaksi`  (
  `idaksi` int(11) NOT NULL AUTO_INCREMENT,
  `create` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `read` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `update` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `delete` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `logtgl` datetime(0) NULL DEFAULT NULL,
  `loguser` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `hargamodal` decimal(10, 0) NULL DEFAULT NULL,
  PRIMARY KEY (`idaksi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbaksi
-- ----------------------------

-- ----------------------------
-- Table structure for tbbarangmasuk
-- ----------------------------
DROP TABLE IF EXISTS `tbbarangmasuk`;
CREATE TABLE `tbbarangmasuk`  (
  `idbarangmasuk` int(11) NOT NULL AUTO_INCREMENT,
  `kodeproduk` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tglmasuk` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nomorinvoice` int(11) NULL DEFAULT NULL,
  `stokmasuk` int(11) NULL DEFAULT NULL,
  `sisastok` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `hargasuplier` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sumberbarang` enum('Pembelian','Suplier','Trading') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `logtgl` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `loguser` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idbarangmasuk`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbbarangmasuk
-- ----------------------------

-- ----------------------------
-- Table structure for tbdepartment
-- ----------------------------
DROP TABLE IF EXISTS `tbdepartment`;
CREATE TABLE `tbdepartment`  (
  `iddept` int(11) NOT NULL AUTO_INCREMENT,
  `namadept` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `logtgl` datetime(0) NULL DEFAULT NULL,
  `loguser` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`iddept`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbdepartment
-- ----------------------------
INSERT INTO `tbdepartment` VALUES (1, 'Personalia', '2022-03-07 12:52:34', NULL);
INSERT INTO `tbdepartment` VALUES (2, 'Marketing', '2022-03-07 12:55:14', NULL);

-- ----------------------------
-- Table structure for tbdetailtransaksi
-- ----------------------------
DROP TABLE IF EXISTS `tbdetailtransaksi`;
CREATE TABLE `tbdetailtransaksi`  (
  `iddetailtransaksi` int(11) NOT NULL AUTO_INCREMENT,
  `jenistransaksi` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kodetransaksi` varchar(13) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kodeproduk` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `qty` int(11) NULL DEFAULT NULL,
  `hargamodal` decimal(10, 0) NULL DEFAULT NULL,
  `hargajual` decimal(10, 0) NULL DEFAULT NULL,
  `hargapublish` decimal(10, 0) NULL DEFAULT NULL,
  `harganet` decimal(10, 0) NULL DEFAULT NULL,
  PRIMARY KEY (`iddetailtransaksi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 61 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbdetailtransaksi
-- ----------------------------
INSERT INTO `tbdetailtransaksi` VALUES (55, 'Pembelian', 'PB03220006', 'D0003', 210, NULL, 3200003, NULL, NULL);
INSERT INTO `tbdetailtransaksi` VALUES (56, 'Pembelian', 'PB03220006', 'FW0001', 122, NULL, 20000, NULL, NULL);
INSERT INTO `tbdetailtransaksi` VALUES (57, 'Pembelian', 'PB03220006', 'P0001', 122, NULL, 22222, NULL, NULL);
INSERT INTO `tbdetailtransaksi` VALUES (58, 'Pembelian', 'PB03220007', 'D0001', 115, NULL, 2000, NULL, NULL);
INSERT INTO `tbdetailtransaksi` VALUES (59, 'Pembelian', 'PB03220007', 'IR0001', 21, NULL, 2000, NULL, NULL);
INSERT INTO `tbdetailtransaksi` VALUES (60, 'Pembelian', 'PB03220008', 'P0002', 2, NULL, 12, NULL, NULL);

-- ----------------------------
-- Table structure for tbdistribusibarang
-- ----------------------------
DROP TABLE IF EXISTS `tbdistribusibarang`;
CREATE TABLE `tbdistribusibarang`  (
  `iddistribusi` int(11) NOT NULL AUTO_INCREMENT,
  `kodeproduk` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `qty` decimal(10, 0) NULL DEFAULT NULL,
  `sumberstore` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tujuanstore` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tglpengiriman` datetime(0) NULL DEFAULT NULL,
  `tglrequest` datetime(0) NULL DEFAULT NULL,
  `statusrequest` enum('Request','Diproses','Terkirim') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `logtgl` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `loguser` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`iddistribusi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbdistribusibarang
-- ----------------------------

-- ----------------------------
-- Table structure for tbidentitasweb
-- ----------------------------
DROP TABLE IF EXISTS `tbidentitasweb`;
CREATE TABLE `tbidentitasweb`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namaweb` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbidentitasweb
-- ----------------------------
INSERT INTO `tbidentitasweb` VALUES (1, 'asas');

-- ----------------------------
-- Table structure for tbjabatan
-- ----------------------------
DROP TABLE IF EXISTS `tbjabatan`;
CREATE TABLE `tbjabatan`  (
  `idjabatan` int(11) NOT NULL AUTO_INCREMENT,
  `namajabatan` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jeniskomisi` enum('Individu','Store') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `logtgl` datetime(0) NULL DEFAULT NULL,
  `loguser` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idjabatan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbjabatan
-- ----------------------------
INSERT INTO `tbjabatan` VALUES (1, 'Manager', NULL, '2022-03-07 01:01:36', NULL);
INSERT INTO `tbjabatan` VALUES (2, 'Supervisor', NULL, '2022-03-07 01:01:47', NULL);
INSERT INTO `tbjabatan` VALUES (4, 'Staff', NULL, '2022-03-07 01:02:00', NULL);
INSERT INTO `tbjabatan` VALUES (5, 'Direktur', NULL, '2022-03-07 01:02:27', NULL);

-- ----------------------------
-- Table structure for tbjenispembayaran
-- ----------------------------
DROP TABLE IF EXISTS `tbjenispembayaran`;
CREATE TABLE `tbjenispembayaran`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenispembayaran` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nominal` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `persent` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbjenispembayaran
-- ----------------------------
INSERT INTO `tbjenispembayaran` VALUES (1, 'Cash', '0', '0');
INSERT INTO `tbjenispembayaran` VALUES (2, 'Manual-Transfer', '100', '2.4');
INSERT INTO `tbjenispembayaran` VALUES (3, 'Credit Card', '0', '0');
INSERT INTO `tbjenispembayaran` VALUES (4, 'Debit Card', '0', '0');

-- ----------------------------
-- Table structure for tbkategori
-- ----------------------------
DROP TABLE IF EXISTS `tbkategori`;
CREATE TABLE `tbkategori`  (
  `kodekategori` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `namakategori` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `logtgl` datetime(0) NULL DEFAULT NULL,
  `loguser` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kodekategori`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbkategori
-- ----------------------------
INSERT INTO `tbkategori` VALUES ('D', 'Driver', '2022-03-12 05:55:23', NULL);
INSERT INTO `tbkategori` VALUES ('F', 'Fullset', '2022-03-18 03:11:09', NULL);
INSERT INTO `tbkategori` VALUES ('FW', 'Fairway Wood', '2022-03-12 05:57:16', NULL);
INSERT INTO `tbkategori` VALUES ('HB', 'Hybrid', '2022-03-18 03:09:18', NULL);
INSERT INTO `tbkategori` VALUES ('I', 'Ironset', '2022-03-12 05:55:56', NULL);
INSERT INTO `tbkategori` VALUES ('IS', 'Iron Satuan', '2022-03-18 03:53:23', NULL);
INSERT INTO `tbkategori` VALUES ('W', 'Wedges', '2022-03-18 03:08:42', NULL);

-- ----------------------------
-- Table structure for tbkodesetting
-- ----------------------------
DROP TABLE IF EXISTS `tbkodesetting`;
CREATE TABLE `tbkodesetting`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kodekategori` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `unit` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `number` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbkodesetting
-- ----------------------------
INSERT INTO `tbkodesetting` VALUES (14, 'D', 'N', 1);
INSERT INTO `tbkodesetting` VALUES (16, 'FW', 'N', 1);
INSERT INTO `tbkodesetting` VALUES (17, 'HB', 'N', 1);
INSERT INTO `tbkodesetting` VALUES (18, 'W', 'N', 1);
INSERT INTO `tbkodesetting` VALUES (19, 'IS', 'N', 2);
INSERT INTO `tbkodesetting` VALUES (20, 'I', 'N', 2);
INSERT INTO `tbkodesetting` VALUES (21, 'D', 'S', 1);

-- ----------------------------
-- Table structure for tbkodesettingtransaksi
-- ----------------------------
DROP TABLE IF EXISTS `tbkodesettingtransaksi`;
CREATE TABLE `tbkodesettingtransaksi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenistransaksi` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `bulan` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tahun` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kodestore` varchar(6) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `number` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbkodesettingtransaksi
-- ----------------------------
INSERT INTO `tbkodesettingtransaksi` VALUES (3, 'PJ', '03', '22', NULL, 21);
INSERT INTO `tbkodesettingtransaksi` VALUES (4, 'PB', '03', '22', NULL, 8);

-- ----------------------------
-- Table structure for tbkomisi
-- ----------------------------
DROP TABLE IF EXISTS `tbkomisi`;
CREATE TABLE `tbkomisi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idjabatan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `periodeawal` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `periodeakhir` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `minimumpenjualan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `maximumpenjualan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `persentasekomisi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `logtgl` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `loguser` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbkomisi
-- ----------------------------

-- ----------------------------
-- Table structure for tbmenu
-- ----------------------------
DROP TABLE IF EXISTS `tbmenu`;
CREATE TABLE `tbmenu`  (
  `idmenu` int(11) NOT NULL AUTO_INCREMENT,
  `idkaryawan` int(11) NULL DEFAULT NULL,
  `idakses` int(11) NULL DEFAULT NULL,
  `namamenu` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `logtgl` datetime(0) NULL DEFAULT NULL,
  `loguser` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idmenu`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbmenu
-- ----------------------------

-- ----------------------------
-- Table structure for tbomsetstore
-- ----------------------------
DROP TABLE IF EXISTS `tbomsetstore`;
CREATE TABLE `tbomsetstore`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kodestore` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `omset` decimal(10, 0) NULL DEFAULT NULL,
  `periodeawal` date NULL DEFAULT NULL,
  `periodeakhir` date NULL DEFAULT NULL,
  `logtgl` datetime(0) NULL DEFAULT NULL,
  `loguser` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbomsetstore
-- ----------------------------

-- ----------------------------
-- Table structure for tbpegawai
-- ----------------------------
DROP TABLE IF EXISTS `tbpegawai`;
CREATE TABLE `tbpegawai`  (
  `idpegawai` int(11) NOT NULL AUTO_INCREMENT,
  `idjabatan` int(11) NULL DEFAULT NULL,
  `iddept` int(11) NULL DEFAULT NULL,
  `kodestore` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `namapegawai` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `logtgl` datetime(0) NULL DEFAULT NULL,
  `loguser` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idpegawai`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbpegawai
-- ----------------------------
INSERT INTO `tbpegawai` VALUES (9, 1, 1, 'B1', 'Rahmat', '2022-03-08 01:04:18', NULL);
INSERT INTO `tbpegawai` VALUES (10, 4, 2, 'B1', 'Budi', '2022-03-08 02:34:20', NULL);

-- ----------------------------
-- Table structure for tbpembayarankonsumen
-- ----------------------------
DROP TABLE IF EXISTS `tbpembayarankonsumen`;
CREATE TABLE `tbpembayarankonsumen`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kodetransaksi` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jenispembayaran` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nomorkartu` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `bankpenerima` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `rekeningpenerima` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `namapenerima` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggalbayar` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 479 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbpembayarankonsumen
-- ----------------------------
INSERT INTO `tbpembayarankonsumen` VALUES (478, 'PB03220008', 'Cash', NULL, NULL, NULL, NULL, '2022-03-17');
INSERT INTO `tbpembayarankonsumen` VALUES (477, 'PB03220007', 'Manual-Transfer', NULL, 'asas', '333222', 'hasan', '2022-03-17');

-- ----------------------------
-- Table structure for tbproduk
-- ----------------------------
DROP TABLE IF EXISTS `tbproduk`;
CREATE TABLE `tbproduk`  (
  `idproduk` int(11) NOT NULL AUTO_INCREMENT,
  `kodekategori` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `unit` enum('S','N') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kodestore` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kodeproduk` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `namaproduk` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `hargamodal` decimal(10, 0) NULL DEFAULT NULL,
  `harganet` decimal(10, 0) NULL DEFAULT NULL,
  `hargapublish` decimal(10, 0) NULL DEFAULT NULL,
  `stokproduk` int(11) NULL DEFAULT NULL,
  `logtgl` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `loguser` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idproduk`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbproduk
-- ----------------------------
INSERT INTO `tbproduk` VALUES (1, 'D', 'S', 'PUSAT', 'D - S0001', 'Driver Yamaha RMX 116 Loft 10,5 shaft speder flex S', '', NULL, 4000000, 4000000, NULL, '2022-03-18 05:10:57', NULL);
INSERT INTO `tbproduk` VALUES (2, 'D', 'N', 'PUSAT', 'SG030621-005', 'Driver Yamaha Rmx loft 10,5 shaft speder 575 flex R', '', NULL, 3300000, 3300000, NULL, '2022-03-18 22:34:11', NULL);
INSERT INTO `tbproduk` VALUES (3, 'FW', 'N', 'PUSAT', 'FW-N0001', 'Fairway Wood 5 RMX Inpres fric off sole 17\' FLEX SR Shaft fubuki', '', NULL, 2800000, 2800000, NULL, '2022-03-18 22:34:14', NULL);
INSERT INTO `tbproduk` VALUES (4, 'FW', 'N', 'PUSAT', 'SG091121-04O', 'Fairway Wood 5 RMX Inpres 18\' FLEX SR Shaft Speeder', '', NULL, 2500000, 2500000, NULL, '2022-03-18 22:34:16', NULL);
INSERT INTO `tbproduk` VALUES (5, 'HB', 'N', 'PUSAT', 'HB-N0001', 'Resque mactec Ut3 loft 20 shaft mactec flex R', '', NULL, 1500000, 1500000, NULL, '2022-03-18 22:34:18', NULL);
INSERT INTO `tbproduk` VALUES (6, 'HB', 'N', 'PUSAT', 'KM061211-027', 'Resque cobra one light loft 19 shaft speder flex S', '', NULL, 3500000, 3500000, NULL, '2022-03-18 22:34:20', NULL);
INSERT INTO `tbproduk` VALUES (7, 'W', 'N', 'PUSAT', 'W-N0001', 'Wedges Slide Zone W 102 loft 53', '', NULL, 1800000, 1800000, NULL, '2022-03-18 22:34:22', NULL);
INSERT INTO `tbproduk` VALUES (8, 'W', 'N', 'PUSAT', 'PS040122-144', 'Wedge PGM G-1 Loft 60 Shaft Steel Original ', '', NULL, 1250000, 1250000, NULL, '2022-03-18 22:34:24', NULL);
INSERT INTO `tbproduk` VALUES (9, 'IS', 'N', 'PUSAT', 'IS - N0002', 'Iron 3 titleist tmb seri 716 shaft dynamic gold', '', NULL, 3000000, 3000000, NULL, '2022-03-18 05:10:32', NULL);
INSERT INTO `tbproduk` VALUES (10, 'I', 'N', 'PUSAT', 'I-N0001', 'Ironset Yamaha inpres X V Forget shaft TOUR AD FLEX R 5-9,P', '', NULL, 7500000, 7500000, NULL, '2022-03-18 23:00:20', NULL);
INSERT INTO `tbproduk` VALUES (11, 'I', 'N', 'PUSAT', 'I-N0002', 'Ironset Cobra onelengh king f9 Shaft NS PRO ZELOS 8 Flex S 5-9,P', '', NULL, 15000000, 15000000, NULL, '2022-03-18 23:01:12', NULL);

-- ----------------------------
-- Table structure for tbrekening
-- ----------------------------
DROP TABLE IF EXISTS `tbrekening`;
CREATE TABLE `tbrekening`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namabank` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nomorrekening` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `namarekening` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbrekening
-- ----------------------------
INSERT INTO `tbrekening` VALUES (1, 'BCA', '112345', 'hasan');
INSERT INTO `tbrekening` VALUES (2, 'Mandiri', '333222', 'Jamal');

-- ----------------------------
-- Table structure for tbstore
-- ----------------------------
DROP TABLE IF EXISTS `tbstore`;
CREATE TABLE `tbstore`  (
  `kodestore` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `wilayah` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `logtgl` datetime(0) NULL DEFAULT NULL,
  `loguser` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kodestore`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbstore
-- ----------------------------
INSERT INTO `tbstore` VALUES ('PBT02', 'Perfecto Bintaro', 'Driving Range Bintaro, Parigi, Kec. Pd. Aren, Kota Tangerang Selatan, Banten 15227', '2022-03-17 02:35:28', NULL);
INSERT INTO `tbstore` VALUES ('PCL04', 'Perfecto Cilandak', 'Cilandak Marinir Driving Range, Jl. Raya Cilandak Kko No.22, Rt.13/5, Cilandak Tim, Kec. Ps. Minggu, Kota Jakarta Selatan, DKI Jakarta, 12560', '2022-03-17 02:36:49', NULL);
INSERT INTO `tbstore` VALUES ('PKM01', 'Perfecto Kemayoran', 'Golf Bandar Kemayoran, Jl. Trembesi Blk. D3-D4, Kota Bandar Baru, Kec. Kemayoran, Kota Jkt Utara, DKI Jakarta 14410', '2022-03-17 02:34:48', NULL);
INSERT INTO `tbstore` VALUES ('PSD05', 'Perfecto BSD', 'Albatross Driving Range Bsd, Lengkong Kulon, Kec. Pagedangan, Kabupaten Tangerang, DKI Jakarta 15339', '2022-03-17 02:38:01', NULL);
INSERT INTO `tbstore` VALUES ('PST03', 'Perfecto Sentul', 'Sentul Highlands Golf Club Driving Range, Cijayanti, Kec. Babakan Madang, Kabupaten Bogor, Jawa Barat, 16810', '2022-03-17 02:38:46', NULL);
INSERT INTO `tbstore` VALUES ('PUSAT', 'CV Perfecto Group', 'Jl Banjaran pucung RT 03/10 Kel. Cilangkap Kec. Tapos, Depok', '2022-03-18 03:20:31', NULL);

-- ----------------------------
-- Table structure for tbtransaksi
-- ----------------------------
DROP TABLE IF EXISTS `tbtransaksi`;
CREATE TABLE `tbtransaksi`  (
  `kodetransaksi` varchar(13) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jenistransaksi` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kodestore` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `totalbayar` int(11) NULL DEFAULT NULL,
  `diskonbelanja` int(11) NULL DEFAULT NULL,
  `idpegawai` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `metodepembayaran` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mdr` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `loguser` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `logtgl` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`kodetransaksi`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbtransaksi
-- ----------------------------
INSERT INTO `tbtransaksi` VALUES ('PB03220007', 'Pembelian', NULL, 272000, 0, '10', '2', '0', NULL, '2022-03-17 01:28:37');
INSERT INTO `tbtransaksi` VALUES ('PB03220008', 'Pembelian', NULL, 24, 0, '10', '1', '0', NULL, '2022-03-17 01:31:05');

-- ----------------------------
-- Table structure for tbtransaksi_temp
-- ----------------------------
DROP TABLE IF EXISTS `tbtransaksi_temp`;
CREATE TABLE `tbtransaksi_temp`  (
  `kodetransaksi` int(11) NOT NULL AUTO_INCREMENT,
  `kodeproduk` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `qty` int(11) NULL DEFAULT NULL,
  `hargajual` int(11) NULL DEFAULT NULL,
  `loguser` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kodetransaksi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 86 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbtransaksi_temp
-- ----------------------------

-- ----------------------------
-- Table structure for tbtransaksibeli_temp
-- ----------------------------
DROP TABLE IF EXISTS `tbtransaksibeli_temp`;
CREATE TABLE `tbtransaksibeli_temp`  (
  `kodetransaksi` int(11) NOT NULL AUTO_INCREMENT,
  `kodekategori` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `qty` int(11) NULL DEFAULT NULL,
  `hargajual` int(11) NULL DEFAULT NULL,
  `loguser` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `namaproduk` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kodetransaksi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 69 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbtransaksibeli_temp
-- ----------------------------

-- ----------------------------
-- Table structure for tbuser
-- ----------------------------
DROP TABLE IF EXISTS `tbuser`;
CREATE TABLE `tbuser`  (
  `username` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `pwd` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `idpegawai` int(11) NULL DEFAULT NULL,
  `level` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbuser
-- ----------------------------
INSERT INTO `tbuser` VALUES ('budi', 'budi123', 10, '');
INSERT INTO `tbuser` VALUES ('rahmat', 'rahmat123', 9, '');

SET FOREIGN_KEY_CHECKS = 1;
